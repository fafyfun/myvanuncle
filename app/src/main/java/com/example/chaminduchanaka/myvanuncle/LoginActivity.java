package com.example.chaminduchanaka.myvanuncle;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bugsnag.android.Bugsnag;
import com.example.chaminduchanaka.myvanuncle.helper.LocaleHelper;
import com.example.chaminduchanaka.myvanuncle.service.SaveData;
import com.example.chaminduchanaka.myvanuncle.service.UsefullData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.CREATE_LOG;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DRIVER_ID;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DRIVER_NAME;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.LOGIN;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.MOBILE;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.PASSWORD;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.VEHICLE;

public class LoginActivity extends AppCompatActivity {
    private RelativeLayout rl_login_content;
    private RelativeLayout rl_lalnguage;
    private LinearLayout ll_copyright;

    private ImageView iv_logo;

    private EditText et_mobile;
    private EditText et_password;

    private TextView tv_reset;
    private TextView tv_forget;

    private Button btn_register;
    private Button btn_login;
    private Button btn_sinhala;
    private Button btn_english;

    private UsefullData usefullData;
    private RequestQueue requestQueue;
    private SaveData saveData;

    Resources resources;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase, "en"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usefullData = new UsefullData(this);
        requestQueue = Volley.newRequestQueue(this);
        saveData = new SaveData(this);

        iv_logo = findViewById(R.id.iv_logo);
        rl_login_content = findViewById(R.id.rl_login_content);
        rl_lalnguage = findViewById(R.id.rl_lalnguage);
        btn_register = findViewById(R.id.btn_register);
        btn_login = findViewById(R.id.btn_login);
        et_mobile = findViewById(R.id.et_mobile);
        btn_sinhala = findViewById(R.id.btn_sinhala);
        btn_english = findViewById(R.id.btn_english);
        et_password = findViewById(R.id.et_password);
        tv_reset = findViewById(R.id.tv_reset);
        tv_forget = findViewById(R.id.tv_forget);
        ll_copyright = findViewById(R.id.ll_copyright);

        Bugsnag.init(this);

        Paper.init(this);

        tv_reset.setPaintFlags(tv_reset.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        //get mobiile and password from the shared preferance
        final String mobile = saveData.retrieveString(MOBILE);
        final String password = saveData.retrieveString(PASSWORD);
        final String driver_id = saveData.retrieveString(DRIVER_ID);

        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                //check shared preferance values
                if (mobile.equals("") || password.equals("") || driver_id.equals("")) {
                    //show login form
                    Animation move = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.logo_animation);
                    iv_logo.startAnimation(move);

                    move.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            rl_lalnguage.setVisibility(View.VISIBLE);
                            ll_copyright.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                } else {
                    //start main activity
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();

                    //create log
                    createLog(saveData.retrieveString(DRIVER_NAME) + "Log to the system");
                }
            }
        }, secondsDelayed * 3000);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_mobile.getText().toString().trim().equals("") || et_password.getText().toString().trim().equals("")) {
                    usefullData.showErrorDialog(resources.getString(R.string.fields_cannot_be_empty));
                } else {
                    btn_login.setEnabled(false);
                    checkLogin();
                }
            }
        });

        tv_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ResetActivity.class);
                startActivity(intent);
            }
        });

        btn_sinhala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Paper.book().write("language", "si");
                updateView((String) Paper.book().read("language"));

                rl_login_content.setVisibility(View.VISIBLE);
                rl_lalnguage.setVisibility(View.GONE);
            }
        });

        btn_english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Paper.book().write("language", "en");
                updateView((String) Paper.book().read("language"));

                rl_login_content.setVisibility(View.VISIBLE);
                rl_lalnguage.setVisibility(View.GONE);
            }
        });
    }

    private void checkLogin() {
        String url = BASE_URL + LOGIN;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("driver");
                                String driver_id = "";
                                String driver_name = "";
                                String driver_vehicle = "";

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    if (object.has("driver_id")) {
                                        driver_id = object.getString("driver_id");
                                    }

                                    if (object.has("driver_fname")) {
                                        driver_name = object.getString("driver_fname");
                                    }

                                    if (object.has("driver_vehicle")) {
                                        driver_vehicle = object.getString("driver_vehicle");
                                    }

                                    try {
                                        if (object.has("driver_lname")) {
                                            driver_name = driver_name + " " + object.getString("driver_lname");
                                        }
                                    } catch (Exception e) {
                                    }
                                }
                                //put values to the shared preferance
                                saveData.saveString(MOBILE, et_mobile.getText().toString());
                                saveData.saveString(PASSWORD, et_password.getText().toString());
                                saveData.saveString(DRIVER_ID, driver_id);
                                saveData.saveString(DRIVER_NAME, driver_name);
                                saveData.saveString(VEHICLE, driver_vehicle);

                                //start main activity
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();

                                //create log
                                createLog(driver_name + "Log to the system");

                            } else {
                                btn_login.setEnabled(true);

                                String message = jsonObject.getString("message");
                                usefullData.showErrorDialog(message);
                            }
                        } catch (JSONException e) {
                            btn_login.setEnabled(true);

                            Log.e("checkLogin()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        btn_login.setEnabled(true);

                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile", et_mobile.getText().toString().trim());
                params.put("password", et_password.getText().toString().trim());
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void createLog(final String msg) {
        String url = BASE_URL + CREATE_LOG;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")) {
//                                usefullData.displayMsg(message);
                            } else {
//                                usefullData.displayMsg(message);
                            }
                        } catch (JSONException e) {
                            Log.e("registerUser()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            if (error instanceof NetworkError) {
                                usefullData.showBottomSheetDialog("Network error occured..!");
                            } else if (error instanceof ServerError) {
                                usefullData.showBottomSheetDialog("Server error occured..!");
                            } else if (error instanceof AuthFailureError) {
                                usefullData.showBottomSheetDialog("Authentication failure..!");
                            } else if (error instanceof NoConnectionError) {
                                usefullData.showBottomSheetDialog("No internet connection..!");
                            } else {
                                usefullData.showBottomSheetDialog("Unknown error occured..!");
                            }
                        } catch (Exception e) {}
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driver_id", saveData.retrieveString(DRIVER_ID));
                params.put("activity", msg);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void updateView(String lang) {
        Context context = LocaleHelper.setLocale(this, lang);
        resources = context.getResources();

        usefullData = new UsefullData(this);

        et_mobile.setHint(resources.getString(R.string.mobile_number));
        et_password.setHint(resources.getString(R.string.password));
        btn_login.setText(resources.getString(R.string.login));
        tv_forget.setText(resources.getString(R.string.forgot));
        tv_reset.setText(resources.getString(R.string.reset));
        btn_register.setText(resources.getString(R.string.register));
    }

}
