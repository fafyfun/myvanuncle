package com.example.chaminduchanaka.myvanuncle;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.myvanuncle.beans.School;
import com.example.chaminduchanaka.myvanuncle.helper.LocaleHelper;
import com.example.chaminduchanaka.myvanuncle.service.GlobalVariables;
import com.example.chaminduchanaka.myvanuncle.service.SaveData;
import com.example.chaminduchanaka.myvanuncle.service.UsefullData;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import fr.ganfra.materialspinner.MaterialSpinner;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.CHILD_ADD;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DRIVER_ID;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.EDIT_CHILD;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.GET_CHILD_DATA;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.GET_SCHOOLS;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.SEND_SMS;

public class EditChildActivity extends AppCompatActivity {

    private UsefullData usefullData;
    private RequestQueue requestQueue;
    private SaveData saveData;

    private String child_id;
    private String school_id;
    private String phone;

    private ArrayList<School> al_school;
    private ArrayList<String> al_distance;

    static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;

    private EditText et_first_name;
    private EditText et_last_name;
    private EditText et_parent_name;
    private EditText et_phone;
    private EditText et_phone_2;
    private EditText et_verification_code;
    private EditText et_lat;
    private EditText et_lng;

    private TextView tv_verification;
    private TextView tv_status;

    private Button btn_copy;
    private Button btn_add_child;
    private Button btn_send_code;
    private Button btn_verify;

    private RelativeLayout rl_verify;
    private LinearLayout ll_verification_code;

    private MaterialSpinner sp_school;
    private MaterialSpinner sp_distance;

    private RotateLoading rotateloading;

    Resources resources;

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_child);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Edit Child");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        usefullData = new UsefullData(this);
        saveData = new SaveData(this);
        requestQueue = Volley.newRequestQueue(this);

        child_id = getIntent().getStringExtra("child_id");

        et_first_name = findViewById(R.id.et_first_name);
        et_last_name = findViewById(R.id.et_last_name);
        et_parent_name = findViewById(R.id.et_parent_name);
        et_phone = findViewById(R.id.et_phone);
        et_phone_2 = findViewById(R.id.et_phone_2);
        et_verification_code = findViewById(R.id.et_verification_code);
        et_lat = findViewById(R.id.et_lat);
        et_lng = findViewById(R.id.et_lng);

        tv_verification = findViewById(R.id.tv_verification);
        tv_status = findViewById(R.id.tv_status);

        btn_copy = findViewById(R.id.btn_copy);
        btn_add_child = findViewById(R.id.btn_add_child);
        btn_send_code = findViewById(R.id.btn_send_code);
        btn_verify = findViewById(R.id.btn_verify);

        sp_school = findViewById(R.id.sp_school);
        sp_distance = findViewById(R.id.sp_distance);

        rl_verify = findViewById(R.id.rl_verify);
        ll_verification_code = findViewById(R.id.ll_verification_code);

        rotateloading = findViewById(R.id.rotateloading);

        Context context = LocaleHelper.onAttach(this);
        resources = context.getResources();

        et_first_name.setHint(resources.getString(R.string.first_name));
        et_last_name.setHint(resources.getString(R.string.last_name));
        et_parent_name.setHint(resources.getString(R.string.parent_name));
        et_phone.setHint(resources.getString(R.string.parent_phone_1));
        btn_send_code.setHint(resources.getString(R.string.send_code));
        btn_send_code.setText(resources.getString(R.string.send_code));
        et_verification_code.setHint(resources.getString(R.string.verification_code));
        btn_verify.setText(resources.getString(R.string.verify));
        tv_status.setText(resources.getString(R.string.status));
        tv_verification.setText(resources.getString(R.string.verification_pending));
        et_phone_2.setHint(resources.getString(R.string.parent_phone_2));
        btn_copy.setText(resources.getString(R.string.copy_current_location));
        et_lat.setHint(resources.getString(R.string.latitude));
        et_lng.setHint(resources.getString(R.string.longitude));
        sp_school.setHint(resources.getString(R.string.school_hint));
        sp_distance.setHint(resources.getString(R.string.select_distance));
        btn_add_child.setText(resources.getString(R.string.save));

        handler = new Handler();

        //set location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        btn_send_code.setEnabled(false);

        btn_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usefullData.hideKeyboard(EditChildActivity.this);
//                getLocation();
                Intent intent = new Intent(EditChildActivity.this, MapsActivity.class);
                startActivity(intent);
            }
        });

        sp_school.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    school_id = al_school.get(position).getSchool_id();
                } catch (Exception e) {}
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_send_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usefullData.hideKeyboard(EditChildActivity.this);

                if (et_phone.getText().toString().trim().equals("")) {
                    usefullData.showErrorDialog(resources.getString(R.string.phone_required));
                } else {
                    //generate code
                    Random random = new Random();
                    String id = String.format("%04d", random.nextInt(10000));
                    saveData.saveString(CHILD_ADD, id);

                    sendSMS(et_phone.getText().toString().trim(), "Your verification code is: " + id);
                }
            }
        });

        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usefullData.hideKeyboard(EditChildActivity.this);

                if (saveData.retrieveString(CHILD_ADD).equals(et_verification_code.getText().toString().trim())) {
                    tv_verification.setText(resources.getString(R.string.verified));
                    tv_verification.setTextColor(getResources().getColor(R.color.login_button));
                } else {
                    usefullData.showErrorDialog(resources.getString(R.string.invalid_verification));
                }
            }
        });

        btn_add_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rotateloading.start();

                boolean is_empty = et_first_name.getText().toString().trim().equals("") ||
                        et_parent_name.getText().toString().trim().equals("") ||
                        et_phone.getText().toString().trim().equals("") ||
                        et_lat.getText().toString().trim().equals("") ||
                        et_lng.getText().toString().trim().equals("") ||
                        sp_school.getSelectedItemPosition() == 0 ||
                        sp_distance.getSelectedItemPosition() == 0;
                if (is_empty) {
                    rotateloading.stop();
                    usefullData.showErrorDialog(resources.getString(R.string.fields_cannot_be_empty));
                } else if (!saveData.retrieveString(CHILD_ADD).equals(et_verification_code.getText().toString().trim())) {
                    if (et_phone.getText().toString().trim().equals(phone)) {
                        updateChild();
                    } else {
                        rotateloading.stop();
                        usefullData.showErrorDialog(resources.getString(R.string.invalid_verification));
                    }
                } else {
                    updateChild();
                }
            }
        });

        getDropDownData();
    }

    @Override
    public void onResume() {

        super.onResume();
        et_lat.setText(GlobalVariables.lat);
        et_lng.setText(GlobalVariables.lng);
    }

    @Override
    public void onPause() {
        super.onPause();
        GlobalVariables.lat = "";
        GlobalVariables.lng = "";
    }

    private void getDropDownData() {
        al_school = new ArrayList<>();

        //get school drop down data
        String url = BASE_URL + GET_SCHOOLS;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Schools", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("success").equals("1")) {
                                JSONArray tmp = jsonObject.getJSONArray("driver");
                                JSONArray schools = tmp.getJSONArray(0);
                                for (int i = 0; i < schools.length(); i++) {
                                    School obj = new School();

                                    JSONObject obj_school = schools.getJSONObject(i);

                                    obj.setSchool_id(obj_school.getString("school_id"));
                                    obj.setSchool_name(obj_school.getString("school_name"));

                                    al_school.add(obj);
                                }
                            }

                            ArrayAdapter<School> adapter_school = new ArrayAdapter<School>(EditChildActivity.this, android.R.layout.simple_spinner_item, al_school);
                            adapter_school.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_school.setAdapter(adapter_school);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driver_id", saveData.retrieveString(DRIVER_ID));

                return params;
            }
        };
        requestQueue.add(stringRequest);

        //set spinner distance values
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.distance));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_distance.setAdapter(adapter);

        loadData();
    }

    public void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();

                et_lat.setText("" + latti);
                et_lng.setText("" + longi);
            }
        }
    }

    private void loadData() {
        String url = BASE_URL + GET_CHILD_DATA;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("school");
                                JSONArray jsonArray1 = jsonArray.getJSONArray(0);
                                JSONObject sch_object = jsonArray1.getJSONObject(0);

                                et_first_name.setText(sch_object.getString("child_fname"));

                                et_last_name.setText(sch_object.getString("child_lname"));

                                et_parent_name.setText(sch_object.getString("child_parent_name"));

                                phone = sch_object.getString("child_parent_phone");
                                et_phone.setText(sch_object.getString("child_parent_phone"));

                                et_phone.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                        if (!et_phone.getText().toString().trim().equals(phone)) {
                                            ll_verification_code.setVisibility(View.VISIBLE);
                                            rl_verify.setVisibility(View.VISIBLE);
                                            btn_send_code.setEnabled(true);
                                        } else {
                                            ll_verification_code.setVisibility(View.GONE);
                                            rl_verify.setVisibility(View.GONE);
                                            btn_send_code.setEnabled(false);
                                        }
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {

                                    }
                                });

                                if (sch_object.has("child_parent_phone_2")) {
                                    et_phone_2.setText(sch_object.getString("child_parent_phone_2"));
                                }

                                et_lng.setText(sch_object.getString("child_lang"));

                                et_lat.setText(sch_object.getString("child_latti"));

                                school_id = sch_object.getString("child_school_id");
                                for (int i = 0; i < al_school.size(); i++) {
                                    if (al_school.get(i).getSchool_id().equals(school_id)) {
                                        sp_school.setSelection(i + 1);
                                    }
                                }

                                if (sch_object.has("child_distance")) {
                                    String distance = sch_object.getString("child_distance");

                                    if (distance.equals("500")) {
                                        sp_distance.setSelection(1);
                                    } else if (distance.equals("1000")) {
                                        sp_distance.setSelection(2);
                                    }
                                }

                            } else {
                                String message = jsonObject.getString("message");
                                usefullData.showErrorDialog(message);
                            }
                        } catch (JSONException e) {
                            Log.e("addSchool()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("child_id", child_id);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void updateChild() {
        String url = BASE_URL + EDIT_CHILD;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        rotateloading.stop();
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")) {

                                usefullData.showSuccessDialog(message);

                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                }, 2000);

                            } else {
                                usefullData.showErrorDialog(message);
                            }
                        } catch (JSONException e) {
                            Log.e("addSchool()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        rotateloading.stop();
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("child_driver_id", saveData.retrieveString(DRIVER_ID));
                params.put("child_fname", et_first_name.getText().toString().trim());
                params.put("child_lname", et_last_name.getText().toString().trim());
                params.put("child_parent_name", et_parent_name.getText().toString().trim());
                params.put("child_parent_phone", et_phone.getText().toString().trim());
                params.put("child_parent_phone_2", et_phone_2.getText().toString().trim());
                params.put("child_lang", et_lng.getText().toString().trim());
                params.put("child_latti", et_lat.getText().toString().trim());
                params.put("child_school_id", school_id);
                params.put("child_id", child_id);

                Log.e("Params", "" + params);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    String number = "";
    public void sendSMS(String phoneNo, final String msg) {
        if (phoneNo.charAt(0) == '0') {
            number = "94" + phoneNo.substring(1);
        } else {
            number = "94" + phoneNo;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SEND_SMS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("SMS response", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("success")) {
                                usefullData.displayMsg("Verification code sent");
                            } else {
                                usefullData.showErrorDialog("Message not sent");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", "10523");
                params.put("api_key", "gN5681YfAwRf3mcezumy");
                params.put("sender_id", "MyVanUncle");
                params.put("to", number);
                params.put("message", msg);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
