package com.example.chaminduchanaka.myvanuncle.fragment;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.myvanuncle.MapsActivity;
import com.example.chaminduchanaka.myvanuncle.R;
import com.example.chaminduchanaka.myvanuncle.beans.School;
import com.example.chaminduchanaka.myvanuncle.helper.LocaleHelper;
import com.example.chaminduchanaka.myvanuncle.service.GlobalVariables;
import com.example.chaminduchanaka.myvanuncle.service.SaveData;
import com.example.chaminduchanaka.myvanuncle.service.UsefullData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import fr.ganfra.materialspinner.MaterialSpinner;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.ADD_CHILD;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.CHILD_ADD;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.CHILD_ADD_2;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.CREATE_LOG;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DRIVER_ID;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.GET_SCHOOLS;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.SEND_SMS;

public class AddChildFragmnet extends Fragment {
    private EditText et_first_name;
    private EditText et_last_name;
    private EditText et_parent_name;
    private EditText et_phone;
    private EditText et_phone_2;
    private EditText et_verification_code;
    private EditText et_verification_code_2;
    private EditText et_lat;
    private EditText et_lng;

    private Button btn_copy;
    private Button btn_add_child;
    private Button btn_send_code;
    private Button btn_send_code_2;
    private Button btn_verify;
    private Button btn_verify_2;

    private TextView tv_verification;
    private TextView tv_verification_2;
    private TextView tv_status;
    private TextView tv_status_2;

    private MaterialSpinner sp_school;
    private MaterialSpinner sp_distance;

    private ArrayList<School> al_school;

    static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;

    String schoolID;

    Resources resources;

    UsefullData usefullData;
    RequestQueue requestQueue;
    SaveData saveData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_child_fragmnet, container, false);

        usefullData = new UsefullData(getContext());
        requestQueue = Volley.newRequestQueue(getContext());
        saveData = new SaveData(getContext());

        et_first_name = view.findViewById(R.id.et_first_name);
        et_last_name = view.findViewById(R.id.et_last_name);
        et_parent_name = view.findViewById(R.id.et_parent_name);
        et_phone = view.findViewById(R.id.et_phone);
        et_phone_2 = view.findViewById(R.id.et_phone_2);
        et_verification_code = view.findViewById(R.id.et_verification_code);
        et_verification_code_2 = view.findViewById(R.id.et_verification_code_2);
        et_lat = view.findViewById(R.id.et_lat);
        et_lng = view.findViewById(R.id.et_lng);

        btn_copy = view.findViewById(R.id.btn_copy);
        btn_add_child = view.findViewById(R.id.btn_add_child);
        btn_send_code = view.findViewById(R.id.btn_send_code);
        btn_send_code_2 = view.findViewById(R.id.btn_send_code_2);
        btn_verify = view.findViewById(R.id.btn_verify);
        btn_verify_2 = view.findViewById(R.id.btn_verify_2);

        tv_verification = view.findViewById(R.id.tv_verification);
        tv_verification_2 = view.findViewById(R.id.tv_verification_2);
        tv_status = view.findViewById(R.id.tv_status);
        tv_status_2 = view.findViewById(R.id.tv_status_2);

        sp_school = view.findViewById(R.id.sp_school);
        sp_distance = view.findViewById(R.id.sp_distance);

        Context context = LocaleHelper.onAttach(getContext());
        resources = context.getResources();

        et_first_name.setHint(resources.getString(R.string.name));
        et_last_name.setHint(resources.getString(R.string.last_name));
        et_parent_name.setHint(resources.getString(R.string.parent_name));
        et_phone.setHint(resources.getString(R.string.parent_phone_1));
        btn_send_code.setText(resources.getString(R.string.send_code));
        btn_send_code_2.setText(resources.getString(R.string.send_code));
        et_verification_code.setHint(resources.getString(R.string.verification_code));
        et_verification_code_2.setHint(resources.getString(R.string.verification_code));
        btn_verify.setText(resources.getString(R.string.verify));
        btn_verify_2.setText(resources.getString(R.string.verify));
        tv_status.setText(resources.getString(R.string.status));
        tv_status_2.setText(resources.getString(R.string.status));
        tv_verification.setText(resources.getString(R.string.verification_pending));
        tv_verification_2.setText(resources.getString(R.string.verification_pending));
        et_phone_2.setHint(resources.getString(R.string.parent_phone_2));
        btn_copy.setText(resources.getString(R.string.copy_current_location));
        et_lat.setHint(resources.getString(R.string.latitude));
        et_lng.setHint(resources.getString(R.string.longitude));
        sp_school.setHint(resources.getString(R.string.school_hint));
        sp_distance.setHint(resources.getString(R.string.select_distance));
        btn_add_child.setText(resources.getString(R.string.add_child));


        btn_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usefullData.hideKeyboard(getActivity());
                Intent intent = new Intent(getActivity(), MapsActivity.class);
                startActivity(intent);
//                getLocation();
            }
        });

        btn_send_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usefullData.hideKeyboard(getActivity());

                if (et_phone.getText().toString().trim().equals("")) {
                    usefullData.showErrorDialog(resources.getString(R.string.phone_required));
                } else {
                    //generate code
                    Random random = new Random();
                    String id = String.format("%04d", random.nextInt(10000));
                    saveData.saveString(CHILD_ADD, id);

                    sendSMS(et_phone.getText().toString().trim(), "Your verification code is: " + id + "\n\n" +
                            "Please visit www.MyVanUncle.lk/terms for Terms and Conditions.");
                }
            }
        });

        btn_send_code_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usefullData.hideKeyboard(getActivity());

                if (et_phone_2.getText().toString().trim().equals("")) {
                    usefullData.showErrorDialog(resources.getString(R.string.phone_required));
                } else {
                    //generate code
                    Random random = new Random();
                    String id = String.format("%04d", random.nextInt(10000));
                    saveData.saveString(CHILD_ADD_2, id);

                    sendSMS(et_phone_2.getText().toString().trim(), "Your verification code is: " + id + "\n\n" +
                            "Please visit www.MyVanUncle.lk/terms for Terms and Conditions.");
                }
            }
        });

        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usefullData.hideKeyboard(getActivity());

                if (et_verification_code.getText().toString().trim().equals("")) {
                    usefullData.showErrorDialog(resources.getString(R.string.invalid_verification));
                } else {
                    if (saveData.retrieveString(CHILD_ADD).equals(et_verification_code.getText().toString().trim())) {
                        tv_verification.setText(resources.getString(R.string.verified));
                        tv_verification.setTextColor(getResources().getColor(R.color.login_button));
                    } else {
                        usefullData.showErrorDialog(resources.getString(R.string.invalid_verification));
                    }
                }
            }
        });

        btn_verify_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usefullData.hideKeyboard(getActivity());

                if (et_verification_code_2.getText().toString().trim().equals("")) {
                    usefullData.showErrorDialog(resources.getString(R.string.invalid_verification));
                } else {
                    if (saveData.retrieveString(CHILD_ADD_2).equals(et_verification_code_2.getText().toString().trim())) {
                        tv_verification_2.setText(resources.getString(R.string.verified));
                        tv_verification_2.setTextColor(getResources().getColor(R.color.login_button));
                    } else {
                        usefullData.showErrorDialog(resources.getString(R.string.invalid_verification));
                    }
                }
            }
        });

        btn_add_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean is_empty = et_first_name.getText().toString().trim().equals("") ||
                        et_parent_name.getText().toString().trim().equals("") ||
                        et_phone.getText().toString().trim().equals("") ||
                        et_lat.getText().toString().trim().equals("") ||
                        et_lng.getText().toString().trim().equals("") ||
                        sp_school.getSelectedItemPosition() == 0 ||
                        sp_distance.getSelectedItemPosition() == 0;
                if (is_empty) {
                    usefullData.showErrorDialog(resources.getString(R.string.fields_cannot_be_empty));
                } else if (!saveData.retrieveString(CHILD_ADD).equals(et_verification_code.getText().toString().trim())) {
                    usefullData.showErrorDialog(resources.getString(R.string.invalid_verification));
                } else {
                    if (et_verification_code_2.getText().length() > 0) {
                        if (saveData.retrieveString(CHILD_ADD_2).equals(et_verification_code_2.getText().toString().trim())) {
                            btn_add_child.setEnabled(false);
                            addChild();
                        } else {
                            usefullData.showErrorDialog(resources.getString(R.string.invalid_verification));
                        }
                    } else {
                        btn_add_child.setEnabled(false);
                        addChild();
                    }
                }
            }
        });

        sp_school.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                usefullData.hideKeyboard(getActivity());
                try {
                    schoolID = al_school.get(position).getSchool_id();
                } catch (Exception e) {}
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        getDropDownData();

        return view;
    }

    @Override
    public void onResume() {

        super.onResume();
        et_lat.setText(GlobalVariables.lat);
        et_lng.setText(GlobalVariables.lng);
    }

    @Override
    public void onPause() {
        super.onPause();
        GlobalVariables.lat = "";
        GlobalVariables.lng = "";
    }

    private void getDropDownData() {
        al_school = new ArrayList<>();

        //get school drop down data
        String url = BASE_URL + GET_SCHOOLS;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Schools", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("success").equals("1")) {
                                JSONArray tmp = jsonObject.getJSONArray("driver");
                                JSONArray schools = tmp.getJSONArray(0);
                                for (int i = 0; i < schools.length(); i++) {
                                    School obj = new School();

                                    JSONObject obj_school = schools.getJSONObject(i);

                                    obj.setSchool_id(obj_school.getString("school_id"));
                                    obj.setSchool_name(obj_school.getString("school_name"));

                                    al_school.add(obj);
                                }
                            }

                            ArrayAdapter<School> adapter_school = new ArrayAdapter<School>(getContext(), android.R.layout.simple_spinner_item, al_school);
                            adapter_school.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_school.setAdapter(adapter_school);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driver_id", saveData.retrieveString(DRIVER_ID));

                return params;
            }
        };
        requestQueue.add(stringRequest);


        //set spinner distance values
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.distance));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_distance.setAdapter(adapter);
    }

    public void getLocation() {

        //set location manager
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();

                et_lat.setText("" + latti);
                et_lng.setText("" + longi);
            }
        }

    }

    private void addChild() {
        String url = BASE_URL + ADD_CHILD;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        btn_add_child.setEnabled(true);
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")) {
                                usefullData.showSuccessDialog(message);

                                //crate log
                                createLog("Name - " + et_first_name.getText().toString().trim() + " / TP - " + et_phone.getText().toString().trim() + " Added to the system by Driver ID - " + saveData.retrieveString(DRIVER_ID));

//                                clearFields();

                                //open school list
                                FragmentManager manager = getActivity().getSupportFragmentManager();
                                FragmentTransaction transaction = manager.beginTransaction();
                                transaction.replace(R.id.frame, new ChildrenListFragment(), "CHILDREN_LIST");
                                transaction.commit();

                            } else {
                                usefullData.showErrorDialog(message);
                            }
                        } catch (JSONException e) {
                            Log.e("addSchool()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        btn_add_child.setEnabled(true);
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("child_driver_id", saveData.retrieveString(DRIVER_ID));
                params.put("child_fname", et_first_name.getText().toString().trim());
                params.put("child_lname", et_last_name.getText().toString().trim());
                params.put("child_parent_name", et_parent_name.getText().toString().trim());
                params.put("child_parent_phone", et_phone.getText().toString().trim());
                params.put("child_parent_phone_2", et_phone_2.getText().toString().trim());
                params.put("child_lang", et_lng.getText().toString().trim());
                params.put("child_latti", et_lat.getText().toString().trim());
                params.put("child_school_id", schoolID);
                params.put("child_distance", sp_distance.getSelectedItem().toString());

                Log.e("Params", "" + params);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void clearFields() {
        et_first_name.setText("");
        et_last_name.setText("");
        et_parent_name.setText("");
        et_phone.setText("");
        et_verification_code.setText("");
        et_lat.setText("");
        et_lng.setText("");
        sp_school.setSelection(0);
        sp_distance.setSelection(0);
    }

    private void createLog(final String msg) {
        String url = BASE_URL + CREATE_LOG;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")) {
//                                usefullData.displayMsg(message);
                            } else {
//                                usefullData.displayMsg(message);
                            }
                        } catch (JSONException e) {
                            Log.e("registerUser()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driver_id", saveData.retrieveString(DRIVER_ID));
                params.put("activity", msg);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    String number = "";
    public void sendSMS(String phoneNo, final String msg) {
        if (phoneNo.charAt(0) == '0') {
            number = "94" + phoneNo.substring(1);
        } else {
            number = "94" + phoneNo;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SEND_SMS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("SMS response", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("success")) {
                                usefullData.displayMsg("Verification code sent");
                            } else {
                                usefullData.showErrorDialog("Message not sent");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", "10523");
                params.put("api_key", "gN5681YfAwRf3mcezumy");
                params.put("sender_id", "MyVanUncle");
                params.put("to", number);
                params.put("message", msg);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

}
