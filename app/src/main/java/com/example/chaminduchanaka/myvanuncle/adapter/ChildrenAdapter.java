package com.example.chaminduchanaka.myvanuncle.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.chaminduchanaka.myvanuncle.R;
import com.example.chaminduchanaka.myvanuncle.beans.Child;
import com.example.chaminduchanaka.myvanuncle.service.SaveData;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.TODAY_CHILDREN_LIST;

public class ChildrenAdapter extends BaseAdapter {
    private ArrayList<Child> list;
    private Activity context;
    private LayoutInflater inflater;
    SaveData saveData;

    public ChildrenAdapter(ArrayList<Child> list, Activity context) {
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        saveData = new SaveData(context);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ChildrenAdapter.ViewHolder holder = null;
        int type = getItemViewType(position);
        if (convertView == null) {
            holder = new ChildrenAdapter.ViewHolder();

            convertView = inflater.inflate(R.layout.model_children, null);
            holder.tv_child_name = (TextView) convertView.findViewById(R.id.tv_child_name);
            holder.tv_school_name = (TextView) convertView.findViewById(R.id.tv_school_name);
            holder.switch_status = (SwitchCompat) convertView.findViewById(R.id.switch_status);
            holder.rl_handle = (RelativeLayout) convertView.findViewById(R.id.rl_handle);

            convertView.setTag(holder);
        } else {
            holder = (ChildrenAdapter.ViewHolder) convertView.getTag();
        }

        if (!list.get(position).getFlag().equals("child")) {
            convertView.setVisibility(View.GONE);
        } else {
            convertView.setVisibility(View.VISIBLE);
        }

        holder.tv_child_name.setText(list.get(position).getName());

        //        holder.tv_school_name.setText(list.get(position).getSchool_name());

        if (list.get(position).getStatus().equals("0")) {
            holder.switch_status.setChecked(false);
        }

        final ViewHolder finalHolder = holder;
        holder.switch_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalHolder.switch_status.isChecked()) {
                    list.get(position).setStatus("1");
                    finalHolder.switch_status.setChecked(true);
                } else {
                    list.get(position).setStatus("0");
                    finalHolder.switch_status.setChecked(false);
                }

                Gson gson = new Gson();
                String today_chidren_list = gson.toJson(list);
                saveData.saveString(TODAY_CHILDREN_LIST, today_chidren_list);
            }
        });

        return convertView;
    }

    public static class ViewHolder {
        public TextView tv_child_name;
        public TextView tv_school_name;
        public SwitchCompat switch_status;
        public RelativeLayout rl_handle;
    }
}
