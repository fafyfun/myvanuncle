package com.example.chaminduchanaka.myvanuncle;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.myvanuncle.helper.LocaleHelper;
import com.example.chaminduchanaka.myvanuncle.service.GlobalVariables;
import com.example.chaminduchanaka.myvanuncle.service.SaveData;
import com.example.chaminduchanaka.myvanuncle.service.UsefullData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DRIVER_ID;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.EDIT_SCHOOL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.GET_SCHOOL_DATA;

public class EditSchoolActivity extends AppCompatActivity {

    private EditText et_school_name;

    private TextView tv_lat;
    private TextView tv_lng;

    private Button btn_google_location;
    private Button btn_add_school;

    static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;

    UsefullData usefullData;
    SaveData saveData;
    RequestQueue requestQueue;

    private String sch_id;

    Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_school);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Edit School");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        usefullData = new UsefullData(this);
        saveData = new SaveData(this);
        requestQueue = Volley.newRequestQueue(this);

        sch_id = getIntent().getStringExtra("sch_id");

        et_school_name = findViewById(R.id.et_school_name);

        tv_lat = findViewById(R.id.tv_lat);
        tv_lng = findViewById(R.id.tv_lng);

        btn_google_location = findViewById(R.id.btn_google_location);
        btn_add_school = findViewById(R.id.btn_add_school);

        Context context = LocaleHelper.onAttach(this);
        resources = context.getResources();

        et_school_name.setHint(resources.getString(R.string.school_name));
        btn_google_location.setText(resources.getString(R.string.copy_current_location));
        tv_lat.setHint(resources.getString(R.string.latitude));
        tv_lng.setHint(resources.getString(R.string.longitude));
        btn_add_school.setText(resources.getString(R.string.save));

        //set location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        btn_google_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getCurrentLocation();
                Intent intent = new Intent(EditSchoolActivity.this, MapsActivity.class);
                startActivity(intent);
            }
        });

        btn_add_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_school_name.getText().toString().trim().equals("") ||
                        tv_lat.getText().toString().trim().equals("") ||
                        tv_lng.getText().toString().trim().equals("")) {
                    usefullData.showErrorDialog(resources.getString(R.string.fields_cannot_be_empty));
                } else {
                    editSchool();
                }
            }
        });

        getSchoolData();
    }

    @Override
    public void onResume() {

        super.onResume();
        tv_lat.setText(GlobalVariables.lat);
        tv_lng.setText(GlobalVariables.lng);
    }

    @Override
    public void onPause() {
        super.onPause();
        GlobalVariables.lat = "";
        GlobalVariables.lng = "";
    }

    private void getCurrentLocation() {
        // check if GPS enabled
        getLocation();
    }

    public void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {

                double latti = location.getLatitude();
                double longi = location.getLongitude();

                tv_lat.setText("" + latti);
                tv_lng.setText("" + longi);

            }
        }
    }

    private void editSchool() {
        String url = BASE_URL + EDIT_SCHOOL;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")) {
                                usefullData.showSuccessDialog(message);

                                finish();

                            } else {
                                usefullData.showErrorDialog(message);
                            }
                        } catch (JSONException e) {
                            Log.e("editSchool()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("school_driver_id", saveData.retrieveString(DRIVER_ID));
                params.put("school_name", et_school_name.getText().toString().trim());
                params.put("school_lang", tv_lng.getText().toString().trim());
                params.put("school_latti", tv_lat.getText().toString().trim());
                params.put("school_id", sch_id);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void getSchoolData() {
        String url = BASE_URL + GET_SCHOOL_DATA;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("school");
                                JSONArray jsonArray1 = jsonArray.getJSONArray(0);
                                JSONObject sch_object = jsonArray1.getJSONObject(0);

                                et_school_name.setText(sch_object.getString("school_name"));

                                tv_lng.setText(sch_object.getString("school_lang"));

                                tv_lat.setText(sch_object.getString("school_latti"));

                            } else {
                                String message = jsonObject.getString("message");
                                usefullData.showErrorDialog(message);
                            }
                        } catch (JSONException e) {
                            Log.e("addSchool()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("school_id", sch_id);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
