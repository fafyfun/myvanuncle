package com.example.chaminduchanaka.myvanuncle;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.myvanuncle.helper.LocaleHelper;
import com.example.chaminduchanaka.myvanuncle.service.SaveData;
import com.example.chaminduchanaka.myvanuncle.service.UsefullData;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.RESET_CODE;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.SELECT_DRIVER;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.SEND_SMS;

public class ResetActivity extends AppCompatActivity {

    private TextView tv_login;
    private TextView tv_gr;
    private TextView tv_remember;

    private EditText et_phone;
    private EditText et_verification_code;

    private Button btn_send_code;
    private Button btn_reset;

    private RotateLoading rotateloading;

    private UsefullData usefullData;
    private RequestQueue requestQueue;
    private SaveData saveData;

    private String driver_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset);

        usefullData = new UsefullData(this);
        requestQueue = Volley.newRequestQueue(this);
        saveData = new SaveData(this);

        tv_login = findViewById(R.id.tv_login);
        tv_gr = findViewById(R.id.tv_gr);
        tv_remember = findViewById(R.id.tv_remember);

        et_phone = findViewById(R.id.et_phone);
        et_verification_code = findViewById(R.id.et_verification_code);

        btn_send_code = findViewById(R.id.btn_send_code);
        btn_reset = findViewById(R.id.btn_reset);

        rotateloading = findViewById(R.id.rotateloading);

        tv_login.setPaintFlags(tv_login.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        Context context = LocaleHelper.onAttach(this);
        final Resources resources = context.getResources();

        et_phone.setHint(resources.getString(R.string.mobile_number));
        btn_send_code.setText(resources.getString(R.string.send_code));
        tv_gr.setText(resources.getString(R.string.kindly_enter));
        et_verification_code.setHint(resources.getString(R.string.verification_code));
        btn_reset.setText(resources.getString(R.string.reset_password));
        tv_remember.setText(resources.getString(R.string.remember));
        tv_login.setText(resources.getString(R.string.login));


        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_send_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_phone.getText().toString().trim().equals("")) {
                    usefullData.showErrorDialog(resources.getString(R.string.phone_required));
                } else {
                    rotateloading.start();
                    checkDriver();
                }
            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_phone.getText().toString().trim().equals("")) {
                    usefullData.showErrorDialog(resources.getString(R.string.phone_required));
                } else if (et_verification_code.getText().toString().trim().equals("")) {
                    usefullData.showErrorDialog(resources.getString(R.string.verification_required));
                } else {
                    checkResetCode();
                }

            }
        });
    }

    private void checkDriver() {
        String url = BASE_URL + SELECT_DRIVER;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")) {
                                //get driver id
                                driver_id = jsonObject.getString("driver_id");

                                //generate code
                                Random random = new Random();
                                String id = String.format("%04d", random.nextInt(10000));
                                saveData.saveString(RESET_CODE, id);

                                //send verification code
                                sendSMS(et_phone.getText().toString().trim(), "Your verification code is: " + id);
                            } else {
                                rotateloading.stop();
                                usefullData.showErrorDialog(message);
                            }
                        } catch (JSONException e) {
                            Log.e("registerUser()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        rotateloading.stop();
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                 params.put("driver_mobile", et_phone.getText().toString().trim());

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    String number = "";
    public void sendSMS(String phoneNo, final String msg) {
        if (phoneNo.charAt(0) == '0') {
            number = "94" + phoneNo.substring(1);
        } else {
            number = "94" + phoneNo;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SEND_SMS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("SMS response", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equals("success")) {
                                rotateloading.stop();
                                usefullData.displayMsg("Verification code sent");
                            } else {
                                usefullData.showErrorDialog("Message not sent");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", "10523");
                params.put("api_key", "gN5681YfAwRf3mcezumy");
                params.put("sender_id", "MyVanUncle");
                params.put("to", number);
                params.put("message", msg);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void checkResetCode() {
        if (et_verification_code.getText().toString().trim().equals(saveData.retrieveString(RESET_CODE))) {
            Intent intent = new Intent(this, NewPasswordActivity.class);
            intent.putExtra("driver_id", driver_id);
            startActivity(intent);
            finish();
        } else {
            usefullData.showErrorDialog("Invalid Verification Code");
        }
    }
}
