package com.example.chaminduchanaka.myvanuncle.fragment;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.myvanuncle.CustomListView;
import com.example.chaminduchanaka.myvanuncle.R;
import com.example.chaminduchanaka.myvanuncle.adapter.ChildrenListAdapter;
import com.example.chaminduchanaka.myvanuncle.beans.Child;
import com.example.chaminduchanaka.myvanuncle.helper.LocaleHelper;
import com.example.chaminduchanaka.myvanuncle.service.SaveData;
import com.example.chaminduchanaka.myvanuncle.service.UsefullData;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DRIVER_ID;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.GET_CHILD;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.UPDATE_POSITION;

public class ChildrenListFragment extends Fragment {

    private UsefullData usefullData;
    private SaveData saveData;
    private RequestQueue requestQueue;

    private CustomListView lv_children;

    private TextView tv_count;
    private TextView tv_children;

    private Button btn_add_child;

    private RotateLoading rotateloading;

    private ArrayList<Child> al_children;

    Resources resources;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_children_list, container, false);

        usefullData = new UsefullData(getActivity());
        saveData = new SaveData(getActivity());
        requestQueue = Volley.newRequestQueue(getActivity());

        lv_children = view.findViewById(R.id.lv_children);

        tv_count = view.findViewById(R.id.tv_count);
        tv_children = view.findViewById(R.id.tv_children);

        btn_add_child = view.findViewById(R.id.btn_add_child);

        rotateloading = view.findViewById(R.id.rotateloading);

        Context context = LocaleHelper.onAttach(getContext());
        resources = context.getResources();

        tv_children.setText(resources.getString(R.string.children));
        btn_add_child.setText(resources.getString(R.string.add_child));

        btn_add_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.frame, new AddChildFragmnet(), "ADD_CHILD");
                transaction.commit();
            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        rotateloading.start();

        getChildrenList();
    }

    private void getChildrenList() {
        al_children = new ArrayList<>();

        String url = BASE_URL + GET_CHILD;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {

                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("child");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    if (object.getString("flag").equals("child")) {
                                        Child child = new Child();
                                        if (object.has("name")) {
                                            child.setName(object.getString("name"));
                                        }

                                        if (object.has("id")) {
                                            child.setId(object.getString("id"));
                                        }

                                        if (object.has("contactNumber")) {
                                            JSONArray array = object.getJSONArray("contactNumber");
                                            String[] numbers = {array.getString(0)};
                                            child.setNumbers(numbers);
                                        }

                                        try {
                                            if (object.has("position")) {
                                                if (!object.getString("position").equals("")) {
                                                    child.setPosition(object.getString("position"));
                                                } else {
                                                    child.setPosition("0");
                                                }
                                            }
                                        } catch (Exception e) {
                                            child.setPosition("0");
                                        }

                                        al_children.add(child);
                                    }
                                }

                                Collections.sort(al_children, new Comparator<Child>() {
                                    @Override
                                    public int compare(Child o1, Child o2) {
                                        return o1.getPosition().compareTo(o2.getPosition());
                                    }
                                });

                                rotateloading.stop();

                                ChildrenListAdapter adapter = new ChildrenListAdapter(al_children, getActivity(), new ChildrenListAdapter.Listener() {
                                    @Override
                                    public void onGrab(int position, RelativeLayout row) {
                                        lv_children.onGrab(position, row);
                                    }
                                });

                                lv_children.setAdapter(adapter);
                                lv_children.setListener(new CustomListView.Listener() {
                                    @Override
                                    public void swapElements(int indexOne, int indexTwo) {
                                        Child temp = al_children.get(indexOne);
                                        al_children.set(indexOne, al_children.get(indexTwo));
                                        al_children.set(indexTwo, temp);

                                        //get position and pass to the database
                                        final JSONArray jsonArray = new JSONArray();
                                        for (int i = 0; i < al_children.size(); i++) {
                                            try {
                                                jsonArray.put(i, al_children.get(i).getId());
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        //pass values to the database
                                        String url = BASE_URL + UPDATE_POSITION;
                                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                                new Response.Listener<String>() {
                                                    @Override
                                                    public void onResponse(String response) {
                                                        Log.e("Position response", response);
                                                    }
                                                },
                                                new Response.ErrorListener() {
                                                    @Override
                                                    public void onErrorResponse(VolleyError error) {
                                                        if (error instanceof NetworkError) {
                                                            usefullData.showBottomSheetDialog("Network error occured..!");
                                                        } else if (error instanceof ServerError) {
                                                            usefullData.showBottomSheetDialog("Server error occured..!");
                                                        } else if (error instanceof AuthFailureError) {
                                                            usefullData.showBottomSheetDialog("Authentication failure..!");
                                                        } else if (error instanceof NoConnectionError) {
                                                            usefullData.showBottomSheetDialog("No internet connection..!");
                                                        } else {
                                                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                                                        }
                                                    }
                                                }) {
                                            @Override
                                            protected Map<String, String> getParams() throws AuthFailureError {
                                                Map<String, String> params = new HashMap<String, String>();
                                                params.put("position", "" + jsonArray);
                                                return params;
                                            }
                                        };
                                        requestQueue.add(stringRequest);
                                    }
                                });

                                tv_count.setText(Integer.toString(al_children.size()));

                            } else {

                                rotateloading.stop();

                                String message = jsonObject.getString("message");
                                usefullData.showErrorDialog(message);

                            }

                        } catch (JSONException e) {

                            rotateloading.stop();

                            Log.e("getCildrenList()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        rotateloading.stop();
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driver_id", saveData.retrieveString(DRIVER_ID));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

}
