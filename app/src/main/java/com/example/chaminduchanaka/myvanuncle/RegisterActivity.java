package com.example.chaminduchanaka.myvanuncle;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.myvanuncle.helper.LocaleHelper;
import com.example.chaminduchanaka.myvanuncle.service.UsefullData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.REGISTER;

public class RegisterActivity extends AppCompatActivity {
    private EditText et_first_name;
    private EditText et_last_name;
    private EditText et_mobile;
    private EditText et_password;
    private EditText et_re_password;
    private EditText et_vehicle_number;

    private Button btn_back;
    private Button btn_sign_up;

    private TextView tv_link;

    private RequestQueue requestQueue;
    private UsefullData usefullData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        requestQueue = Volley.newRequestQueue(this);
        usefullData = new UsefullData(this);

        et_first_name = findViewById(R.id.et_first_name);
        et_last_name = findViewById(R.id.et_last_name);
        et_mobile = findViewById(R.id.et_mobile);
        et_password = findViewById(R.id.et_password);
        et_re_password = findViewById(R.id.et_re_password);
        et_vehicle_number = findViewById(R.id.et_vehicle_number);

        tv_link = findViewById(R.id.tv_link);
        tv_link.setPaintFlags(tv_link.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        btn_back = findViewById(R.id.btn_back);
        btn_sign_up = findViewById(R.id.btn_sign_up);

        Context context = LocaleHelper.onAttach(this);
        final Resources resources = context.getResources();

        et_first_name.setHint(resources.getString(R.string.first_name));
        et_last_name.setHint(resources.getString(R.string.last_name));
        et_mobile.setHint(resources.getString(R.string.mobile_number_reg));
        et_password.setHint(resources.getString(R.string.password));
        et_re_password.setHint(resources.getString(R.string.re_enter_password));
        et_vehicle_number.setHint(resources.getString(R.string.vehicle_number));
        btn_sign_up.setText(resources.getString(R.string.sign_up));
        btn_back.setText(resources.getString(R.string.back_to_login));

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_first_name.getText().toString().trim().equals("") ||
                        et_last_name.getText().toString().trim().equals("") ||
                        et_mobile.getText().toString().trim().equals("") ||
                        et_password.getText().toString().trim().equals("") ||
                        et_re_password.getText().toString().trim().equals("") ||
                        et_vehicle_number.getText().toString().trim().equals("")) {

                    usefullData.showErrorDialog(resources.getString(R.string.fields_cannot_be_empty));
                } else {
                    String password = et_password.getText().toString();
                    String re_password = et_re_password.getText().toString();
                    if (password.equals(re_password)) {
                        registerUser();
                    } else {
                        usefullData.showErrorDialog(resources.getString(R.string.passwords_didnt_match));
                    }
                }

            }
        });

        tv_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://www.MyVanUncle.lk/terms";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
    }

    private void registerUser() {
        String url = BASE_URL + REGISTER;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")) {
                                usefullData.showSuccessDialog(message);
                                clearFields();
                            } else {
                                usefullData.showErrorDialog(message);
                            }
                        } catch (JSONException e) {
                            Log.e("registerUser()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driver_fname", et_first_name.getText().toString().trim());
                params.put("driver_lname", et_last_name.getText().toString().trim());
                params.put("driver_mobile", et_mobile.getText().toString().trim());
                params.put("driver_passowrd", et_password.getText().toString().trim());
                params.put("driver_vehicle", et_vehicle_number.getText().toString().trim());

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void clearFields() {
        et_first_name.setText("");
        et_last_name.setText("");
        et_mobile.setText("");
        et_password.setText("");
        et_re_password.setText("");
        et_vehicle_number.setText("");
    }
}
