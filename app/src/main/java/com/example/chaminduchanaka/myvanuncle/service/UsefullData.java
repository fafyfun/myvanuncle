package com.example.chaminduchanaka.myvanuncle.service;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chaminduchanaka.myvanuncle.R;
import com.example.chaminduchanaka.myvanuncle.helper.LocaleHelper;

public class UsefullData {
    Context context;
    Resources resources;

    public UsefullData(Context context) {
        this.context = context;
        Context c = LocaleHelper.onAttach(context);
        resources = c.getResources();
    }

    public void displayMsg(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public void showSuccessDialog(String msg){
        final Dialog dialog = new Dialog(context, R.style.dialog_theme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.success_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.tv_message);
        TextView tv_success = (TextView) dialog.findViewById(R.id.tv_success);
        text.setText(msg);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_close);

        tv_success.setText(resources.getString(R.string.success));
        dialogButton.setText(resources.getString(R.string.close));

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void showErrorDialog(String msg){
        final Dialog dialog = new Dialog(context, R.style.dialog_theme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.error_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.tv_message);
        TextView tv_error = (TextView) dialog.findViewById(R.id.tv_error);
        text.setText(msg);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_close);

        tv_error.setText(resources.getString(R.string.error));
        dialogButton.setText(resources.getString(R.string.try_again));

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void limitDialog(String msg) {
        final Dialog dialog = new Dialog(context, R.style.dialog_theme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.success_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.tv_message);
        text.setText(msg);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_close);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

        // Hide after some seconds
        final Handler handler  = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        };

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
            }
        });

        handler.postDelayed(runnable, 3000);
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showBottomSheetDialog(String message) {
        BottomSheetDialog dialog = new BottomSheetDialog(context);
        dialog.setContentView(R.layout.bottom_sheet);
        TextView textView = dialog.findViewById(R.id.tv_error);
        textView.setText(message);
        dialog.show();
    }

}
