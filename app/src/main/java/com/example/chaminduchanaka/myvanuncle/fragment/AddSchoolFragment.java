package com.example.chaminduchanaka.myvanuncle.fragment;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.myvanuncle.MapsActivity;
import com.example.chaminduchanaka.myvanuncle.R;
import com.example.chaminduchanaka.myvanuncle.helper.LocaleHelper;
import com.example.chaminduchanaka.myvanuncle.service.GlobalVariables;
import com.example.chaminduchanaka.myvanuncle.service.SaveData;
import com.example.chaminduchanaka.myvanuncle.service.UsefullData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.ADD_SCHOOL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.CREATE_LOG;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DRIVER_ID;

public class AddSchoolFragment extends Fragment {
    private EditText et_school_name;
    private EditText tv_lat;
    private EditText tv_lng;

    private Button btn_google_location;
    private Button btn_add_school;

    static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;

    String driver_id = "";

    UsefullData usefullData;
    SaveData saveData;
    RequestQueue requestQueue;

    Resources resources;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_school, container, false);

        usefullData = new UsefullData(getContext());
        saveData = new SaveData(getContext());
        requestQueue = Volley.newRequestQueue(getContext());

        et_school_name = view.findViewById(R.id.et_school_name);
        tv_lat = view.findViewById(R.id.tv_lat);
        tv_lng = view.findViewById(R.id.tv_lng);

        btn_google_location = view.findViewById(R.id.btn_google_location);
        btn_add_school = view.findViewById(R.id.btn_add_school);

        Context context = LocaleHelper.onAttach(getContext());
        resources = context.getResources();

        et_school_name.setHint(resources.getString(R.string.school_name));
        btn_google_location.setText(resources.getString(R.string.copy_current_location));
        tv_lat.setHint(resources.getString(R.string.latitude));
        tv_lng.setHint(resources.getString(R.string.longitude));
        btn_add_school.setText(resources.getString(R.string.add_school));


        btn_google_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usefullData.hideKeyboard(getActivity());
//                getCurrentLocation();
                Intent intent = new Intent(getActivity(), MapsActivity.class);
                startActivity(intent);
            }
        });

        btn_add_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_school_name.getText().toString().trim().equals("") ||
                        tv_lat.getText().toString().trim().equals("") ||
                        tv_lng.getText().toString().trim().equals("")) {
                    usefullData.showErrorDialog(resources.getString(R.string.fields_cannot_be_empty));
                } else {
                    addSchool();
                }
            }
        });

        //get driver id
        driver_id = saveData.retrieveString(DRIVER_ID);

        return view;
    }

    @Override
    public void onResume() {

        super.onResume();
        tv_lat.setText(GlobalVariables.lat);
        tv_lng.setText(GlobalVariables.lng);
    }

    @Override
    public void onPause() {
        super.onPause();
        GlobalVariables.lat = "";
        GlobalVariables.lng = "";
    }

    private void getCurrentLocation() {
        // check if GPS enabled
        getLocation();
    }

    public void getLocation() {
        //set location manager
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {

                double latti = location.getLatitude();
                double longi = location.getLongitude();

                tv_lat.setText("" + latti);
                tv_lng.setText("" + longi);

            }
        }
    }

    private void addSchool() {
        String url = BASE_URL + ADD_SCHOOL;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")) {
                                usefullData.showSuccessDialog(message);

                                //crate log
                                createLog(et_school_name.getText().toString().trim() + " Added to the system");

//                                clearFields();

                                //open school list
                                FragmentManager manager = getActivity().getSupportFragmentManager();
                                FragmentTransaction transaction = manager.beginTransaction();
                                transaction.replace(R.id.frame, new SchoolListFragment(), "SCHOOL_LIST");
                                transaction.commit();

                            } else {
                                usefullData.showErrorDialog(message);
                            }
                        } catch (JSONException e) {
                            Log.e("addSchool()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("school_driver_id", driver_id);
                params.put("school_name", et_school_name.getText().toString().trim());
                params.put("school_lang", tv_lng.getText().toString().trim());
                params.put("school_latti", tv_lat.getText().toString().trim());

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void clearFields() {
        et_school_name.setText("");
        tv_lng.setText("");
        tv_lat.setText("");
    }

    private void createLog(final String msg) {
        String url = BASE_URL + CREATE_LOG;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")) {
//                                usefullData.displayMsg(message);
                            } else {
//                                usefullData.displayMsg(message);
                            }
                        } catch (JSONException e) {
                            Log.e("registerUser()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driver_id", saveData.retrieveString(DRIVER_ID));
                params.put("activity", msg);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

}
