package com.example.chaminduchanaka.myvanuncle.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.myvanuncle.R;
import com.example.chaminduchanaka.myvanuncle.service.SaveData;
import com.example.chaminduchanaka.myvanuncle.service.UsefullData;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.LOGIN;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.MOBILE;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.PASSWORD;

public class DriverProfileFragment extends Fragment {

    private TextView tv_driver_name;
    private TextView tv_driver_mobile;
    private TextView tv_driver_vehicle;

    private UsefullData usefullData;
    private SaveData saveData;
    private RequestQueue requestQueue;

    private RotateLoading rotateloading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_profile, container, false);

        usefullData = new UsefullData(getActivity());
        saveData = new SaveData(getActivity());
        requestQueue = Volley.newRequestQueue(getActivity());

        tv_driver_name = view.findViewById(R.id.tv_driver_name);
        tv_driver_mobile = view.findViewById(R.id.tv_driver_mobile);
        tv_driver_vehicle = view.findViewById(R.id.tv_driver_vehicle);

        rotateloading = view.findViewById(R.id.rotateloading);

        rotateloading.start();
        getDriverProfile();

        return view;
    }

    private void getDriverProfile() {
        String url = BASE_URL + LOGIN;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;

                        rotateloading.stop();

                        try {

                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("driver");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String driver_name = "";
                                    if (object.has("driver_fname")) {
                                        driver_name = object.getString("driver_fname");
                                    }

                                    if (object.has("driver_lname")) {
                                        driver_name = driver_name + " " + object.getString("driver_lname");
                                    }

                                    if (object.has("driver_vehicle")) {
                                        tv_driver_vehicle.setText("Vehicle Number: " + object.getString("driver_vehicle"));
                                    }

                                    tv_driver_name.setText("Name: " + driver_name);
                                    tv_driver_mobile.setText("Mobile: " + saveData.retrieveString(MOBILE));
                                }

                            } else {

                                rotateloading.stop();

                                String message = jsonObject.getString("message");
                                usefullData.showErrorDialog(message);

                            }

                        } catch (JSONException e) {

                            rotateloading.stop();

                            Log.e("getCildrenList()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        rotateloading.stop();
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile", saveData.retrieveString(MOBILE));
                params.put("password", saveData.retrieveString(PASSWORD));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

}
