package com.example.chaminduchanaka.myvanuncle.fragment;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.myvanuncle.R;
import com.example.chaminduchanaka.myvanuncle.adapter.ChildrenAdapter;
import com.example.chaminduchanaka.myvanuncle.beans.Child;
import com.example.chaminduchanaka.myvanuncle.helper.LocaleHelper;
import com.example.chaminduchanaka.myvanuncle.service.SaveData;
import com.example.chaminduchanaka.myvanuncle.service.UsefullData;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.CREATE_LOG;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DRIVER_ID;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DRIVER_NAME;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.GET_CHILD;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.GET_QUOTES;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.INSERT_LOCATION;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.SEND_SMS;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.TODAY_CHILDREN_LIST;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.VEHICLE;

public class DropOffFragment extends Fragment {
    SaveData saveData;
    UsefullData usefullData;
    RequestQueue requestQueue;

    private Button btn_start_evening;

    ArrayList<Child> al_today_children;

    ArrayList<String> today_children_number;

    private ListView lv_child;


    private static final String TAG = "PICK_UP_FRAGMENT";

    /**
     * Code used in requesting runtime permissions.
     */
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    /**
     * Constant used in the location settings dialog.
     */
    private static final int REQUEST_CHECK_SETTINGS = 0x1;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    private final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    private final static String KEY_LOCATION = "location";
    private final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";

    int REQUEST_SEND_SMS = 1;

    /**
     * Provides access to the Fused Locationlist Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Provides access to the Locationlist Settings API.
     */
    private SettingsClient mSettingsClient;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    private LocationRequest mLocationRequest;

    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    private LocationSettingsRequest mLocationSettingsRequest;

    /**
     * Callback for Locationlist events.
     */
    private LocationCallback mLocationCallback;

    /**
     * Represents a geographical location.
     */
    private Location mCurrentLocation;

    // Labels.
    private String mLatitudeLabel;
    private String mLongitudeLabel;
    private String mLastUpdateTimeLabel;

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    private Boolean mRequestingLocationUpdates;

    /**
     * Time when the location was updated represented as a String.
     */
    private String mLastUpdateTime;

    SmsManager smsManager = SmsManager.getDefault();

    private TextView tv_count;
    private TextView tv_children;

    Resources resources;

    Handler handler;

    private LinearLayout ll_activated;

    private RotateLoading rotateloading;

    private String msg3 = "";
    private String msg4 = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_drop_off, container, false);

        lv_child = view.findViewById(R.id.lv_child_drop);

        btn_start_evening = view.findViewById(R.id.btn_start_evening);
        ll_activated = view.findViewById(R.id.ll_activated);
        rotateloading = view.findViewById(R.id.rotateloading);

        ll_activated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_activated.setVisibility(View.GONE);
            }
        });

        saveData = new SaveData(getActivity());

        usefullData = new UsefullData(getActivity());

        requestQueue = Volley.newRequestQueue(getActivity());

        tv_count = view.findViewById(R.id.tv_count);
        tv_children = view.findViewById(R.id.tv_children);

        Context context = LocaleHelper.onAttach(getContext());
        resources = context.getResources();

        tv_children.setText(resources.getString(R.string.children));

        handler = new Handler();

        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //get SMS quotes
        getQuotes();

        openDialog();

        btn_start_evening.setText(resources.getString(R.string.end_evening_drive));

        btn_start_evening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().getSupportFragmentManager().beginTransaction().
                        remove(getActivity().getSupportFragmentManager().findFragmentById(R.id.frame)).commit();

                getActivity().findViewById(R.id.ll_buttons).setVisibility(View.VISIBLE);

                //remove the children list cache
                saveData.saveString(TODAY_CHILDREN_LIST, "");

                createLog(saveData.retrieveString(DRIVER_NAME) + " end the evening drive");

            }
        });

        rotateloading.start();
        getChildrenList();

        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";

        // Update values using data stored in the Bundle.
        updateValuesFromBundle(savedInstanceState);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mSettingsClient = LocationServices.getSettingsClient(getActivity());

        // Kick off the process of building the LocationCallback, LocationRequest, and
        // LocationSettingsRequest objects.
        createLocationCallback();
        createLocationRequest();
        buildLocationSettingsRequest();

        return view;
    }

    private void openDialog() {
        final Dialog dialog = new Dialog(getContext(), R.style.dialog_theme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.alert_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.tv_message);
        TextView tv_alert = (TextView) dialog.findViewById(R.id.tv_alert);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok);

        tv_alert.setText(resources.getString(R.string.alert));
        text.setText(resources.getString(R.string.evening_attendance));
        dialogButton.setText(resources.getString(R.string.ok));

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Within {@code onPause()}, we remove location updates. Here, we resume receiving
                        // location updates if the user has requested them.
                        if (mRequestingLocationUpdates && checkPermissions()) {
                            startLocationUpdates();
                        } else if (!checkPermissions()) {
                            requestPermissions();
                        }

                        updateUI();
                    }
                }, 5000);
            }
        });

        dialog.show();
    }

    private void getChildrenList() {
        try {
            Gson gson = new Gson();
            String list = saveData.retrieveString(TODAY_CHILDREN_LIST);

            if (list.equals("")) {
                childList();
            } else {
                al_today_children = gson.fromJson(list, new TypeToken<List<Child>>() {
                }.getType());

                getChildrenCount(al_today_children);

                setAdapter();
            }

        } catch (Exception e) {
            childList();
        }
    }

    private void setAdapter() {
        rotateloading.stop();

        Log.e("Children size", "" + al_today_children.size());

        ChildrenAdapter childrenAdapter = new ChildrenAdapter(al_today_children, getActivity());
        lv_child.setAdapter(childrenAdapter);

    }

    private void childList() {
        al_today_children = new ArrayList<>();

        String url = BASE_URL + GET_CHILD;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        try {
                            JSONObject object = new JSONObject(response);
                            JSONArray jsonArray = object.getJSONArray("child");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                if (jsonObject.getString("flag").equals("child")) {
                                    Child obj = new Child();

                                    if (jsonObject.has("id")) {
                                        obj.setId(jsonObject.getString("id"));
                                    }

                                    if (jsonObject.has("name")) {
                                        obj.setName(jsonObject.getString("name"));
                                    }

                                    if (jsonObject.has("long")) {
                                        obj.setLng(jsonObject.getString("long"));
                                    }

                                    if (jsonObject.has("latt")) {
                                        obj.setLat(jsonObject.getString("latt"));
                                    }

                                    if (jsonObject.has("contactNumber")) {
                                        JSONArray numArray = jsonObject.getJSONArray("contactNumber");
                                        String[] numbers = {numArray.getString(0)};
                                        obj.setNumbers(numbers);
                                    }

                                    if (jsonObject.has("flag")) {
                                        obj.setFlag(jsonObject.getString("flag"));
                                    }

                                    if (jsonObject.has("distance")) {
                                        if (!jsonObject.getString("distance").equals("")) {
                                            obj.setDistance(jsonObject.getString("distance"));
                                        } else {
                                            obj.setDistance("0");
                                        }
                                    } else {
                                        obj.setDistance("0");
                                    }

                                    if (jsonObject.has("contactNumber2")) {
                                        if (!jsonObject.getString("contactNumber2").equals("")) {
                                            obj.setNumbers2(jsonObject.getString("contactNumber2"));
                                        } else {
                                            obj.setNumbers2("");
                                        }
                                    } else {
                                        obj.setNumbers2("");
                                    }

                                    obj.setStatus("1");

                                    obj.setTag("1");

                                    if (jsonObject.has("position")) {
                                        if (!jsonObject.getString("position").equals("")) {
                                            obj.setPosition(jsonObject.getString("position"));
                                        } else {
                                            obj.setPosition("0");
                                        }
                                    }

                                    al_today_children.add(obj);

                                    Collections.sort(al_today_children, new Comparator<Child>() {
                                        @Override
                                        public int compare(Child o1, Child o2) {
                                            return o1.getPosition().compareTo(o2.getPosition());
                                        }
                                    });
                                }
                            }

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                if (jsonObject.getString("flag").equals("school")) {
                                    Child obj = new Child();

                                    if (jsonObject.has("id")) {
                                        obj.setId(jsonObject.getString("id"));
                                    }

                                    if (jsonObject.has("name")) {
                                        obj.setName(jsonObject.getString("name"));
                                    }

                                    if (jsonObject.has("long")) {
                                        obj.setLng(jsonObject.getString("long"));
                                    }

                                    if (jsonObject.has("latt")) {
                                        obj.setLat(jsonObject.getString("latt"));
                                    }

                                    try {
                                        if (jsonObject.has("contactNumber")) {
                                            JSONArray numArray = jsonObject.getJSONArray("contactNumber");
                                            String[] numbers = new String[numArray.length() - 1];
                                            for (int j = 0; j < numArray.length(); j++) {
                                                if (!numArray.getString(j).equals("")) {
                                                    numbers[j] = numArray.getString(j);
                                                }
                                            }
                                            obj.setNumbers(numbers);
                                        }
                                    } catch (Exception e) {
                                    }

                                    if (jsonObject.has("flag")) {
                                        obj.setFlag(jsonObject.getString("flag"));
                                    }

                                    if (jsonObject.has("contactNumber2")) {
                                        if (!jsonObject.getString("contactNumber2").equals("")) {
                                            obj.setNumbers2(jsonObject.getString("contactNumber2"));
                                        } else {
                                            obj.setNumbers2("");
                                        }
                                    } else {
                                        obj.setNumbers2("");
                                    }

                                    obj.setStatus("1");

                                    obj.setTag("1");

                                    al_today_children.add(obj);
                                }
                            }

                            getChildrenCount(al_today_children);

                            setAdapter();

                        } catch (JSONException e) {
                            Log.e("Pick up fragment", e.getMessage());
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        rotateloading.stop();
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driver_id", saveData.retrieveString(DRIVER_ID));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void getChildrenCount(ArrayList<Child> list) {
        int count = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getFlag().equals("child")) {
                count++;
            }

        }
        tv_count.setText("" + count);
    }

    int a = 0;

    private void getTodayChildren() {
        today_children_number = new ArrayList<>();

        for (int i = 0; i < al_today_children.size(); i++) {
            if (al_today_children.get(i).getFlag().equals("child")) {
                if (al_today_children.get(i).getStatus().equals("1")) {
                    String[] tmp = al_today_children.get(i).getNumbers();
                    today_children_number.add(tmp[0].toString());
                }
            }
        }

        if (a == 0) {
//            Toast.makeText(getActivity(), "Location tracking activated", Toast.LENGTH_LONG).show();
            ll_activated.setVisibility(View.VISIBLE);
            startLocationUpdates();
        }
        a++;
    }

    /**
     * Updates fields based on data stored in the bundle.
     *
     * @param savedInstanceState The activity state saved in the Bundle.
     */
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                // Since KEY_LOCATION was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }

            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
            updateUI();
        }
    }

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Locationlist Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Creates a callback for receiving location events.
     */
    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                updateLocationUI();

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        uploadLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
                    }
                }, 30000);
            }
        };
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        mRequestingLocationUpdates = false;
                        updateUI();
                        break;
                }
                break;
        }
    }

    /**
     * Handles the Start Updates button and requests start of location updates. Does nothing if
     * updates have already been requested.
     */
    public void startUpdatesButtonHandler(View view) {
        if (!mRequestingLocationUpdates) {
            mRequestingLocationUpdates = true;
            setButtonsEnabledState();
            startLocationUpdates();
        }
    }

    /**
     * Handles the Stop Updates button, and requests removal of location updates.
     */
    public void stopUpdatesButtonHandler(View view) {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        stopLocationUpdates();
    }

    /**
     * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
     * runtime permission has been granted.
     */
    private void startLocationUpdates() {
        // Begin by checking if the device has the necessary location settings.
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());

                        updateUI();
                    }
                })
                .addOnFailureListener(getActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Locationlist settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Locationlist settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);
                                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                                mRequestingLocationUpdates = false;
                        }

                        updateUI();
                    }
                });
    }

    /**
     * Updates all UI fields.
     */
    private void updateUI() {
        setButtonsEnabledState();
        updateLocationUI();
    }

    /**
     * Disables both buttons when functionality is disabled due to insuffucient location settings.
     * Otherwise ensures that only one button is enabled at any time. The Start Updates button is
     * enabled if the user is not requesting location updates. The Stop Updates button is enabled
     * if the user is requesting location updates.
     */
    private void setButtonsEnabledState() {
//        if (mRequestingLocationUpdates) {
//            mStartUpdatesButton.setEnabled(false);
//            mStopUpdatesButton.setEnabled(true);
//            mLastUpdateTimeTextView.setText("Process Started");
//        } else {
//            mStartUpdatesButton.setEnabled(true);
//            mStopUpdatesButton.setEnabled(false);
//            mLastUpdateTimeTextView.setText("Process End");
//        }
    }

    /**
     * Sets the value of the UI fields for the location latitude, longitude and last update time.
     */
    private void updateLocationUI() {
        getTodayChildren();

        requestSmsPermission();
        if (mCurrentLocation != null) {

            String dDistance = "";

            for (int i = 0; i < al_today_children.size(); i++) {

                //get current location as start point
                Location startPoint = new Location("locationA");
                startPoint.setLatitude(mCurrentLocation.getLatitude());
                startPoint.setLongitude(mCurrentLocation.getLongitude());

                //get child home location as end point
                Location endPoint = new Location("locationA");
                endPoint.setLatitude(Double.parseDouble(al_today_children.get(i).getLat()));
                endPoint.setLongitude(Double.parseDouble(al_today_children.get(i).getLng()));

                //calculate distance
                int distance = (int) startPoint.distanceTo(endPoint);

                dDistance = dDistance + distance;

                //check variables
                Log.e("Start point", "" + startPoint);
                Log.e("End point", "" + endPoint);
                Log.e("Distance", dDistance);
                Log.e("Tag", al_today_children.get(i).getTag());
                Log.e("---", "-------------------------------");

                if (al_today_children.get(i).getTag().equals("1")) {
                    //check sms sending the first time

                    try {

                        if (al_today_children.get(i).getFlag().equals("school")) {

                            if (distance <= 200) {
                                final String[] numbers = al_today_children.get(i).getNumbers();

                                Log.e("Length in school", "" + numbers.length);
                                Log.e("Length in today", "" + today_children_number.size());

                                if ((numbers.length) == today_children_number.size()) {
                                    for (int j = 0; j < numbers.length; j++) {
                                        final String num = numbers[j];

                                        Log.e("Phone School", num);

                                        final String message = saveData.retrieveString(VEHICLE) + " is " + distance + "m away from " + al_today_children.get(i).getName() + "\n\n" + msg3;
//                                            smsManager.sendTextMessage(num, null, message, null, null);
//                                            sendSMS(num, message);


                                        String temp_number = "";
                                        if (num.charAt(0) == '0') {
                                            temp_number = "94" + num.substring(1);
                                        } else {
                                            temp_number = "94" + num;
                                        }

                                        final String finalTemp_number = temp_number;
                                        StringRequest stringRequest = new StringRequest(Request.Method.POST, SEND_SMS,
                                                new Response.Listener<String>() {
                                                    @Override
                                                    public void onResponse(String response) {
                                                        Log.e("SMS response", response);

                                                        //display message
                                                        usefullData.limitDialog("SMS Sent");

                                                        //crateLog
                                                        createLog("SMS sent to the " + num + "/Near School");
                                                    }
                                                },
                                                new Response.ErrorListener() {
                                                    @Override
                                                    public void onErrorResponse(VolleyError error) {
                                                        if (error instanceof NetworkError) {
                                                            usefullData.showBottomSheetDialog("Network error occured..!");
                                                        } else if (error instanceof ServerError) {
                                                            usefullData.showBottomSheetDialog("Server error occured..!");
                                                        } else if (error instanceof AuthFailureError) {
                                                            usefullData.showBottomSheetDialog("Authentication failure..!");
                                                        } else if (error instanceof NoConnectionError) {
                                                            usefullData.showBottomSheetDialog("No internet connection..!");
                                                        } else {
                                                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                                                        }
                                                    }
                                                }) {
                                            @Override
                                            protected Map<String, String> getParams() throws AuthFailureError {
                                                Map<String, String> params = new HashMap<String, String>();
                                                params.put("user_id", "10523");
                                                params.put("api_key", "gN5681YfAwRf3mcezumy");
                                                params.put("sender_id", "MyVanUncle");
                                                params.put("to", finalTemp_number);
                                                params.put("message", message);

                                                Log.e("Params", "" + params);

                                                return params;
                                            }
                                        };
                                        requestQueue.add(stringRequest);


                                        //send message to the second number
                                        try {

                                            String temp_number_1 = "";
                                            if (num.charAt(0) == '0') {
                                                temp_number_1 = "94" + al_today_children.get(j).getNumbers2().substring(1);
                                            } else {
                                                temp_number_1 = "94" + al_today_children.get(j).getNumbers2();
                                            }

                                            final String finalTemp_number_1 = temp_number_1;
                                            final int finalJ = j;
                                            StringRequest stringRequest_1 = new StringRequest(Request.Method.POST, SEND_SMS,
                                                    new Response.Listener<String>() {
                                                        @Override
                                                        public void onResponse(String response) {
                                                            Log.e("SMS response", response);

                                                            //display message
                                                            usefullData.limitDialog("SMS Sent");

                                                            //crateLog
                                                            createLog("SMS sent to the " + al_today_children.get(finalJ).getNumbers2() + "/Near School");
                                                        }
                                                    },
                                                    new Response.ErrorListener() {
                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {
                                                            if (error instanceof NetworkError) {
                                                                usefullData.showBottomSheetDialog("Network error occured..!");
                                                            } else if (error instanceof ServerError) {
                                                                usefullData.showBottomSheetDialog("Server error occured..!");
                                                            } else if (error instanceof AuthFailureError) {
                                                                usefullData.showBottomSheetDialog("Authentication failure..!");
                                                            } else if (error instanceof NoConnectionError) {
                                                                usefullData.showBottomSheetDialog("No internet connection..!");
                                                            } else {
                                                                usefullData.showBottomSheetDialog("Unknown error occured..!");
                                                            }
                                                        }
                                                    }) {
                                                @Override
                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                    Map<String, String> params = new HashMap<String, String>();
                                                    params.put("user_id", "10523");
                                                    params.put("api_key", "gN5681YfAwRf3mcezumy");
                                                    params.put("sender_id", "MyVanUncle");
                                                    params.put("to", finalTemp_number_1);
                                                    params.put("message", message);

                                                    Log.e("Params", "" + params);

                                                    return params;
                                                }
                                            };
                                            requestQueue.add(stringRequest_1);


                                        } catch (Exception e) {
                                            Log.e("Number 2 error", e.getMessage());
                                        }


                                    }
                                } else {
                                    for (int j = 0; j < today_children_number.size(); j++) {
                                        for (int k = 0; k < (numbers.length); k++) {
                                            if (today_children_number.get(j).equals(numbers[k])) {
                                                Log.e("Phone school", numbers[k]);

                                                final String message = saveData.retrieveString(VEHICLE) + " is " + distance + "m away from " + al_today_children.get(i).getName() + "\n\n" + msg3;
//                                                    smsManager.sendTextMessage(numbers[k], null, message, null, null);
//                                                    sendSMS(numbers[k], message);


                                                String temp_number = "";
                                                if (numbers[k].charAt(0) == '0') {
                                                    temp_number = "94" + numbers[k].substring(1);
                                                } else {
                                                    temp_number = "94" + numbers[k];
                                                }

                                                final String finalTemp_number = temp_number;
                                                final int finalK = k;
                                                StringRequest stringRequest = new StringRequest(Request.Method.POST, SEND_SMS,
                                                        new Response.Listener<String>() {
                                                            @Override
                                                            public void onResponse(String response) {
                                                                Log.e("SMS response", response);

                                                                //display message
                                                                usefullData.limitDialog("SMS Sent");

                                                                //crateLog
                                                                createLog("SMS sent to the " + numbers[finalK] + "/Near school");
                                                            }
                                                        },
                                                        new Response.ErrorListener() {
                                                            @Override
                                                            public void onErrorResponse(VolleyError error) {
                                                                if (error instanceof NetworkError) {
                                                                    usefullData.showBottomSheetDialog("Network error occured..!");
                                                                } else if (error instanceof ServerError) {
                                                                    usefullData.showBottomSheetDialog("Server error occured..!");
                                                                } else if (error instanceof AuthFailureError) {
                                                                    usefullData.showBottomSheetDialog("Authentication failure..!");
                                                                } else if (error instanceof NoConnectionError) {
                                                                    usefullData.showBottomSheetDialog("No internet connection..!");
                                                                } else {
                                                                    usefullData.showBottomSheetDialog("Unknown error occured..!");
                                                                }
                                                            }
                                                        }) {
                                                    @Override
                                                    protected Map<String, String> getParams() throws AuthFailureError {
                                                        Map<String, String> params = new HashMap<String, String>();
                                                        params.put("user_id", "10523");
                                                        params.put("api_key", "gN5681YfAwRf3mcezumy");
                                                        params.put("sender_id", "MyVanUncle");
                                                        params.put("to", finalTemp_number);
                                                        params.put("message", message);

                                                        Log.e("Params", "" + params);

                                                        return params;
                                                    }
                                                };
                                                requestQueue.add(stringRequest);


                                                //send message to the second number
                                                try {

                                                    String temp_number_1 = "";
                                                    if (numbers[k].charAt(0) == '0') {
                                                        temp_number_1 = "94" + al_today_children.get(k).getNumbers2().substring(1);
                                                    } else {
                                                        temp_number_1 = "94" + al_today_children.get(k).getNumbers2();
                                                    }

                                                    final String finalTemp_number_1 = temp_number_1;
                                                    final int finalK1 = k;
                                                    StringRequest stringRequest_1 = new StringRequest(Request.Method.POST, SEND_SMS,
                                                            new Response.Listener<String>() {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    Log.e("SMS response", response);

                                                                    //display message
                                                                    usefullData.limitDialog("SMS Sent");

                                                                    //crateLog
                                                                    createLog("SMS sent to the " + al_today_children.get(finalK1).getNumbers2() + "/Near School");
                                                                }
                                                            },
                                                            new Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error) {
                                                                    if (error instanceof NetworkError) {
                                                                        usefullData.showBottomSheetDialog("Network error occured..!");
                                                                    } else if (error instanceof ServerError) {
                                                                        usefullData.showBottomSheetDialog("Server error occured..!");
                                                                    } else if (error instanceof AuthFailureError) {
                                                                        usefullData.showBottomSheetDialog("Authentication failure..!");
                                                                    } else if (error instanceof NoConnectionError) {
                                                                        usefullData.showBottomSheetDialog("No internet connection..!");
                                                                    } else {
                                                                        usefullData.showBottomSheetDialog("Unknown error occured..!");
                                                                    }
                                                                }
                                                            }) {
                                                        @Override
                                                        protected Map<String, String> getParams() throws AuthFailureError {
                                                            Map<String, String> params = new HashMap<String, String>();
                                                            params.put("user_id", "10523");
                                                            params.put("api_key", "gN5681YfAwRf3mcezumy");
                                                            params.put("sender_id", "MyVanUncle");
                                                            params.put("to", finalTemp_number_1);
                                                            params.put("message", message);

                                                            Log.e("Params", "" + params);

                                                            return params;
                                                        }
                                                    };
                                                    requestQueue.add(stringRequest_1);


                                                } catch (Exception e) {
                                                    Log.e("Number 2 error", e.getMessage());
                                                }

                                            }
                                        }
                                    }
                                }

                                al_today_children.get(i).setTag("0");
                            }

                        } else if (al_today_children.get(i).getFlag().equals("child")) {

                            if (distance <= Integer.parseInt(al_today_children.get(i).getDistance())) {
                                String[] numbers = al_today_children.get(i).getNumbers();

                                Log.e("Length", "" + numbers.length);

                                if (al_today_children.get(i).getStatus().equals("1")) {

                                    final String num = numbers[0];

                                    Log.e("Phone", num);

                                    final String message = saveData.retrieveString(VEHICLE) + " is " + distance + "m away from " + al_today_children.get(i).getName() + "'s location\n\n" + msg4;
//                                        smsManager.sendTextMessage(num, null, message, null, null);
//                                        sendSMS(num, message);


                                    String temp_number = "";
                                    if (num.charAt(0) == '0') {
                                        temp_number = "94" + num.substring(1);
                                    } else {
                                        temp_number = "94" + num;
                                    }

                                    final String finalTemp_number = temp_number;
                                    StringRequest stringRequest = new StringRequest(Request.Method.POST, SEND_SMS,
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    Log.e("SMS response", response);

                                                    //display message
                                                    usefullData.limitDialog("SMS Sent");

                                                    //crateLog
                                                    createLog("SMS sent to the " + num + "/Near home");
                                                }
                                            },
                                            new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    if (error instanceof NetworkError) {
                                                        usefullData.showBottomSheetDialog("Network error occured..!");
                                                    } else if (error instanceof ServerError) {
                                                        usefullData.showBottomSheetDialog("Server error occured..!");
                                                    } else if (error instanceof AuthFailureError) {
                                                        usefullData.showBottomSheetDialog("Authentication failure..!");
                                                    } else if (error instanceof NoConnectionError) {
                                                        usefullData.showBottomSheetDialog("No internet connection..!");
                                                    } else {
                                                        usefullData.showBottomSheetDialog("Unknown error occured..!");
                                                    }
                                                }
                                            }) {
                                        @Override
                                        protected Map<String, String> getParams() throws AuthFailureError {
                                            Map<String, String> params = new HashMap<String, String>();
                                            params.put("user_id", "10523");
                                            params.put("api_key", "gN5681YfAwRf3mcezumy");
                                            params.put("sender_id", "MyVanUncle");
                                            params.put("to", finalTemp_number);
                                            params.put("message", message);

                                            Log.e("Params", "" + params);

                                            return params;
                                        }
                                    };
                                    requestQueue.add(stringRequest);


                                    try {
//                                            sendSMS(al_today_children.get(i).getNumbers2(), message);

                                        String temp_number_1 = "";
                                        if (num.charAt(0) == '0') {
                                            temp_number_1 = "94" + al_today_children.get(i).getNumbers2().substring(1);
                                        } else {
                                            temp_number_1 = "94" + al_today_children.get(i).getNumbers2();
                                        }

                                        final String finalTemp_number_1 = temp_number_1;
                                        final int finalI = i;
                                        StringRequest stringRequest_1 = new StringRequest(Request.Method.POST, SEND_SMS,
                                                new Response.Listener<String>() {
                                                    @Override
                                                    public void onResponse(String response) {
                                                        Log.e("SMS response", response);

                                                        //display message
                                                        usefullData.limitDialog("SMS Sent");

                                                        //crateLog
                                                        createLog("SMS sent to the " + al_today_children.get(finalI).getNumbers2() + "/Near home");
                                                    }
                                                },
                                                new Response.ErrorListener() {
                                                    @Override
                                                    public void onErrorResponse(VolleyError error) {
                                                        if (error instanceof NetworkError) {
                                                            usefullData.showBottomSheetDialog("Network error occured..!");
                                                        } else if (error instanceof ServerError) {
                                                            usefullData.showBottomSheetDialog("Server error occured..!");
                                                        } else if (error instanceof AuthFailureError) {
                                                            usefullData.showBottomSheetDialog("Authentication failure..!");
                                                        } else if (error instanceof NoConnectionError) {
                                                            usefullData.showBottomSheetDialog("No internet connection..!");
                                                        } else {
                                                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                                                        }
                                                    }
                                                }) {
                                            @Override
                                            protected Map<String, String> getParams() throws AuthFailureError {
                                                Map<String, String> params = new HashMap<String, String>();
                                                params.put("user_id", "10523");
                                                params.put("api_key", "gN5681YfAwRf3mcezumy");
                                                params.put("sender_id", "MyVanUncle");
                                                params.put("to", finalTemp_number_1);
                                                params.put("message", message);

                                                Log.e("Params", "" + params);

                                                return params;
                                            }
                                        };
                                        requestQueue.add(stringRequest_1);


                                    } catch (Exception e) {
                                        Log.e("Number 2 error", e.getMessage());
                                    }


                                    al_today_children.get(i).setTag("0");

                                }

                            }

                        }


                    } catch (Exception ex) {
                        ex.printStackTrace();

                        al_today_children.get(i).setTag("0");

                    }
                }

            }
        }
    }

    private void uploadLocation(final double latitude, final double longitude) {

        String url = BASE_URL + INSERT_LOCATION;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Location add response", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driver_id", saveData.retrieveString(DRIVER_ID));
                params.put("lattitude", "" + latitude);
                params.put("longitude", "" + longitude);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void requestSmsPermission() {
        try {
            String permission = Manifest.permission.SEND_SMS;
            int grant = ContextCompat.checkSelfPermission(getActivity(), permission);
            if (grant != PackageManager.PERMISSION_GRANTED) {
                String[] permission_list = new String[1];
                permission_list[0] = permission;
                ActivityCompat.requestPermissions(getActivity(), permission_list, 1);
            }
        } catch (Exception e) {
        }

    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    private void stopLocationUpdates() {
        if (!mRequestingLocationUpdates) {
            Log.d(TAG, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }

        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mRequestingLocationUpdates = false;
                        setButtonsEnabledState();
                    }
                });
    }

    @Override
    public void onPause() {
        super.onPause();

        // Remove location updates to save battery.
        stopLocationUpdates();
    }

    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
//        Snackbar.make(
//                findViewById(android.R.id.content),
//                getString(mainTextStringId),
//                Snackbar.LENGTH_INDEFINITE)
//                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
//            showSnackbar(R.string.permission_rationale,
//                    android.R.string.ok, new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            // Request permission
//                            ActivityCompat.requestPermissions(MainActivity.this,
//                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                                    REQUEST_PERMISSIONS_REQUEST_CODE);
//                        }
//                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (mRequestingLocationUpdates) {
                    Log.i(TAG, "Permission granted, updates requested, starting location updates");
                    startLocationUpdates();
                }
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
//                showSnackbar(R.string.permission_denied_explanation,
//                        R.string.settings, new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                // Build intent that displays the App settings screen.
//                                Intent intent = new Intent();
//                                intent.setAction(
//                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                                Uri uri = Uri.fromParts("package",
//                                        BuildConfig.APPLICATION_ID, null);
//                                intent.setData(uri);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(intent);
//                            }
//                        });
            }
        }
    }

    private void createLog(final String msg) {
        String url = BASE_URL + CREATE_LOG;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")) {
//                                usefullData.displayMsg(message);
                            } else {
//                                usefullData.displayMsg(message);
                            }
                        } catch (JSONException e) {
                            Log.e("registerUser()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driver_id", saveData.retrieveString(DRIVER_ID));
                params.put("activity", msg);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    String number = "";

    private void sendSMS(final String to, final String msg) {
        if (to.charAt(0) == '0') {
            number = "94" + to.substring(1);
        } else {
            number = "94" + to;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SEND_SMS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("SMS response", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.displayMsg("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMsg("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMsg("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMsg("No internet connection..!");
                        } else {
                            usefullData.displayMsg("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", "10523");
                params.put("api_key", "gN5681YfAwRf3mcezumy");
                params.put("sender_id", "MyVanUncle");
                params.put("to", number);
                params.put("message", msg);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void getQuotes() {

        //get current date
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        final String formattedDate = df.format(c);

        String url = BASE_URL + GET_QUOTES;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {

                                JSONArray quot = jsonObject.getJSONArray("quotes");
                                JSONObject quot_obj = quot.getJSONObject(0);

                                if (quot_obj.has("msg3")) {
                                    msg3 = quot_obj.getString("msg3");
                                }

                                if (quot_obj.has("msg4")) {
                                    msg4 = quot_obj.getString("msg4");
                                }
                            }
                        } catch (JSONException e) {
                            Log.e("registerUser()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("date", formattedDate);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

}
