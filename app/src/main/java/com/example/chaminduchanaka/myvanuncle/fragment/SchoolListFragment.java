package com.example.chaminduchanaka.myvanuncle.fragment;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.myvanuncle.R;
import com.example.chaminduchanaka.myvanuncle.adapter.SchoolListAdapter;
import com.example.chaminduchanaka.myvanuncle.beans.School;
import com.example.chaminduchanaka.myvanuncle.helper.LocaleHelper;
import com.example.chaminduchanaka.myvanuncle.service.SaveData;
import com.example.chaminduchanaka.myvanuncle.service.UsefullData;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DRIVER_ID;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.GET_SCHOOLS;

public class SchoolListFragment extends Fragment {

    private Button btn_add_school;

    private UsefullData usefullData;
    private SaveData saveData;
    private RequestQueue requestQueue;

    private TextView tv_count;
    private TextView tv_schools;

    private ListView lv_schools;

    private RotateLoading rotateloading;

    private ArrayList<School> al_schools;

    Resources resources;

    SchoolListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_school_list, container, false);

        usefullData = new UsefullData(getActivity());
        saveData = new SaveData(getActivity());
        requestQueue = Volley.newRequestQueue(getActivity());

        tv_count = view.findViewById(R.id.tv_count);
        tv_schools = view.findViewById(R.id.tv_schools);

        lv_schools = view.findViewById(R.id.lv_schools);

        btn_add_school = view.findViewById(R.id.btn_add_school);

        rotateloading = view.findViewById(R.id.rotateloading);

        Context context = LocaleHelper.onAttach(getContext());
        resources = context.getResources();

        tv_schools.setText(resources.getString(R.string.school));
        btn_add_school.setText(resources.getString(R.string.add_school));

        btn_add_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.frame, new AddSchoolFragment(), "ADD_SCHOOL");
                transaction.commit();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        rotateloading.start();

        getSchoolList();
    }

    private void getSchoolList() {
        al_schools = new ArrayList<>();

        String url = BASE_URL + GET_SCHOOLS;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {

                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("driver");
                                JSONArray jsonArray_schools = jsonArray.getJSONArray(0);

                                for (int i = 0; i < jsonArray_schools.length(); i++) {
                                    JSONObject object = jsonArray_schools.getJSONObject(i);
                                    School school = new School();
                                    if (object.has("school_name")) {
                                        school.setSchool_name(object.getString("school_name"));
                                    }

                                    if (object.has("school_id")) {
                                        school.setSchool_id(object.getString("school_id"));
                                    }

                                    al_schools.add(school);

                                }

                                rotateloading.stop();

                                adapter = new SchoolListAdapter(al_schools, getActivity());

                                lv_schools.setAdapter(adapter);

                                tv_count.setText(Integer.toString(al_schools.size()));

                            } else {

                                rotateloading.stop();
                                String message = jsonObject.getString("message");
                                usefullData.showErrorDialog(message);

                            }

                        } catch (JSONException e) {
                            rotateloading.stop();
                            Log.e("getSchoolList()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        rotateloading.stop();
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driver_id", saveData.retrieveString(DRIVER_ID));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
