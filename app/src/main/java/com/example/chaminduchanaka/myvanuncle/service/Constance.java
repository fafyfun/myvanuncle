package com.example.chaminduchanaka.myvanuncle.service;

public interface Constance {
    //Shared preferance keys
    String PREFS_NAME = "myvanuncle";
    String DRIVER_ID = "driver_id";
    String DRIVER_NAME = "driver_name";
    String MOBILE = "mobile";
    String VEHICLE = "vehicle";
    String PASSWORD = "password";
    String TODAY_CHILDREN_LIST = "today_children_list";


    //to get which fragment open
    String START_OR_END = "start_or_end";
    String RIDE_TIME = "ride_time";

    //delete keys
    String DELETE_CODE = "delete_code";


    //reset key
    String RESET_CODE = "reset_code";

    //child add key
    String CHILD_ADD = "child_add";
    String CHILD_ADD_2 = "child_add_2";

    //Server keys
    String BASE_URL = "http://myvanuncle.lk/app/";
    String REGISTER = "add_driver.php";
    String LOGIN = "login.php";
    String ADD_SCHOOL = "add_school.php";
    String GET_SCHOOLS = "select_school.php";
    String ADD_CHILD = "add_child.php";
    String GET_CHILD = "getchild.php";
    String CREATE_LOG = "createLog.php";
    String DELETE_CHILD = "delete_child.php";
    String DELETE_SCHOOL = "delete_school.php";
    String GET_SCHOOL_DATA = "get_school_data.php";
    String EDIT_SCHOOL = "edit_school.php";
    String GET_CHILD_DATA = "get_child_data.php";
    String EDIT_CHILD = "edit_child.php";
    String SELECT_DRIVER = "select_driver.php";
    String RESET_PASSWORD = "reset_password.php";
    String GET_QUOTES = "getQuot.php";
    String UPDATE_POSITION = "update_position.php";
    String INSERT_LOCATION = "insertLocation.php";


    //send sms
    String SEND_SMS = "https://app.notify.lk/api/v1/send";
}
