package com.example.chaminduchanaka.myvanuncle.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.myvanuncle.EditChildActivity;
import com.example.chaminduchanaka.myvanuncle.R;
import com.example.chaminduchanaka.myvanuncle.beans.Child;
import com.example.chaminduchanaka.myvanuncle.helper.LocaleHelper;
import com.example.chaminduchanaka.myvanuncle.service.SaveData;
import com.example.chaminduchanaka.myvanuncle.service.UsefullData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.CREATE_LOG;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DELETE_CHILD;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DELETE_CODE;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DRIVER_ID;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.SEND_SMS;

public class ChildrenListAdapter extends BaseAdapter {
    private ArrayList<Child> list;
    private Activity context;
    private LayoutInflater inflater;

    private SaveData saveData;
    private UsefullData usefullData;
    private RequestQueue requestQueue;

    private Resources resources;

    final int INVALID_ID = -1;

    public interface Listener {
        void onGrab(int position, RelativeLayout row);
    }

    final Listener listener;
    final Map<Child, Integer> mIdMap = new HashMap<>();

    public ChildrenListAdapter(ArrayList<Child> list, Activity context, Listener listener) {
        this.list = list;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        saveData = new SaveData(context);
        usefullData = new UsefullData(context);
        requestQueue = Volley.newRequestQueue(context);

        Context c = LocaleHelper.onAttach(context);
        resources = c.getResources();

        this.listener = listener;
        this.list = list;
        for (int i = 0; i < list.size(); i++) {
            mIdMap.put(list.get(i), i);
        }
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Child getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        if (position < 0 || position >= mIdMap.size()) {
            return INVALID_ID;
        }
        Child item = getItem(position);
        return mIdMap.get(item);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        SchoolListAdapter.ViewHolder holder = null;
        int type = getItemViewType(position);
        if (convertView == null) {
            holder = new SchoolListAdapter.ViewHolder();

            convertView = inflater.inflate(R.layout.model_children_list, null);
            holder.tv_school_name = (TextView) convertView.findViewById(R.id.tv_school_name);
            holder.rl_delete = (RelativeLayout) convertView.findViewById(R.id.rl_delete);
            holder.rl_edit = (RelativeLayout) convertView.findViewById(R.id.rl_edit);
            holder.rl_handle = (RelativeLayout) convertView.findViewById(R.id.rl_handle);

            convertView.setTag(holder);
        } else {
            holder = (SchoolListAdapter.ViewHolder) convertView.getTag();
        }

        final RelativeLayout row = (RelativeLayout) convertView.findViewById(R.id.child_row);

        holder.tv_school_name.setText(list.get(position).getName());

        holder.rl_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context, R.style.dialog_theme);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.delete_dialog);

                final LinearLayout ll_more = dialog.findViewById(R.id.ll_more);
                final View view = dialog.findViewById(R.id.view);
                TextView text = (TextView) dialog.findViewById(R.id.tv_name);
                TextView tv_delete = (TextView) dialog.findViewById(R.id.tv_delete);
                TextView tv_message = (TextView) dialog.findViewById(R.id.tv_message);
                final Button dialogButton = (Button) dialog.findViewById(R.id.btn_cancel);
                final Button btnProceed = (Button) dialog.findViewById(R.id.btn_proceed);
                final EditText et_verification_code = dialog.findViewById(R.id.et_verification_code);
                Button btn_delete = dialog.findViewById(R.id.btn_delete);

                tv_delete.setText(resources.getString(R.string.delete_text));
                tv_message.setText(resources.getString(R.string.you_are_requesting));
                btnProceed.setText(resources.getString(R.string.proceed));
                btn_delete.setText(resources.getString(R.string.delete_btn));
                dialogButton.setText(resources.getString(R.string.cancel));
                et_verification_code.setHint(resources.getString(R.string.verification_code));

                text.setText(list.get(position).getName());

                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                btnProceed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //show more details
                        ll_more.setVisibility(View.VISIBLE);
                        btnProceed.setVisibility(View.GONE);
                        view.setVisibility(View.GONE);

                        //delete process

                        //generate random number
                        Random random = new Random();
                        String id = String.format("%04d", random.nextInt(10000));
                        saveData.saveString(DELETE_CODE, id);

                        //send sms
                        sendSMS(list.get(position).getNumbers()[0], "Your verification code is: " + id);
                    }
                });

                btn_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //check verification code
                        if (et_verification_code.getText().toString().trim().equals(saveData.retrieveString(DELETE_CODE))) {
                            String url = BASE_URL + DELETE_CHILD;
                            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            Log.e("Response", response);
                                            JSONObject jsonObject = null;
                                            try {

                                                jsonObject = new JSONObject(response);
                                                String success = jsonObject.getString("success");
                                                String message = jsonObject.getString("message");
                                                if (success.equals("1")) {
                                                    createLog("Child ID - " + list.get(position).getId()  + " Deleted from the system by Driver ID - " + saveData.retrieveString(DRIVER_ID));

                                                    dialog.dismiss();
                                                    usefullData.showSuccessDialog(message);
                                                    list.remove(position);
                                                    notifyDataSetChanged();
                                                } else {
                                                    usefullData.showErrorDialog(message);
                                                }

                                            } catch (JSONException e) {
                                                Log.e("getCildrenList()", "Json Error");
                                                e.printStackTrace();
                                            }
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            if (error instanceof NetworkError) {
                                                usefullData.displayMsg("Network error occured..!");
                                            } else if (error instanceof ServerError) {
                                                usefullData.displayMsg("Network error occured..!");
                                            } else if (error instanceof AuthFailureError) {
                                                usefullData.displayMsg("Authentication failure..!");
                                            } else if (error instanceof NoConnectionError) {
                                                usefullData.displayMsg("No internet connection..!");
                                            } else {
                                                usefullData.displayMsg("Unknown error occured..!");
                                            }
                                        }
                                    }) {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("child_id", list.get(position).getId());
                                    return params;
                                }
                            };
                            requestQueue.add(stringRequest);
                        } else {
                            Toast.makeText(context,"Invalid verification code",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });

                dialog.show();
            }
        });

        holder.rl_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EditChildActivity.class);
                intent.putExtra("child_id", list.get(position).getId());
                context.startActivity(intent);
            }
        });

        holder.rl_handle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                listener.onGrab(position, row);
                return false;
            }
        });

        return convertView;
    }

    String number = "";
    public void sendSMS(String phoneNo, final String msg) {
        if (phoneNo.charAt(0) == '0') {
            number = "94" + phoneNo.substring(1);
        } else {
            number = "94" + phoneNo;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SEND_SMS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("SMS response", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.displayMsg("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMsg("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMsg("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMsg("No internet connection..!");
                        } else {
                            usefullData.displayMsg("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", "10523");
                params.put("api_key", "gN5681YfAwRf3mcezumy");
                params.put("sender_id", "NotifyDEMO");
                params.put("to", number);
                params.put("message", msg);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    private void createLog(final String msg) {
        String url = BASE_URL + CREATE_LOG;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")) {
//                                usefullData.displayMsg(message);
                            } else {
//                                usefullData.displayMsg(message);
                            }
                        } catch (JSONException e) {
                            Log.e("registerUser()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.displayMsg("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.displayMsg("Network error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.displayMsg("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.displayMsg("No internet connection..!");
                        } else {
                            usefullData.displayMsg("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driver_id", saveData.retrieveString(DRIVER_ID));
                params.put("activity", msg);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    public static class ViewHolder {
        public TextView tv_school_name;
        public RelativeLayout rl_edit;
        public RelativeLayout rl_delete;
        public RelativeLayout rl_handle;
    }
}
