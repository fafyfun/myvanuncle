package com.example.chaminduchanaka.myvanuncle.service;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.PREFS_NAME;

public class SaveData {
    Context context;

    public SaveData(Context context) {
        this.context = context;
    }

    public void saveString(String key, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String retrieveString(String key) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString(key, "");
        return restoredText;
    }
}
