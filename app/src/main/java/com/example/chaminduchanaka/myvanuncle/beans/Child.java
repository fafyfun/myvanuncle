package com.example.chaminduchanaka.myvanuncle.beans;

public class Child {
    private String id;
    private String name;
    private String lat;
    private String lng;
    private String[] numbers;
    private String numbers2;
    private String flag;
    private String status;
    private String tag;
    private String distance;
    private String position;

    public Child(String id, String name, String lat, String lng, String[] numbers, String flag, String status, String tag) {
        this.id = id;
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.numbers = numbers;
        this.flag = flag;
        this.status = status;
        this.tag = tag;
    }

    public Child() {}

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String[] getNumbers() {
        return numbers;
    }

    public void setNumbers(String[] numbers) {
        this.numbers = numbers;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getNumbers2() {
        return numbers2;
    }

    public void setNumbers2(String numbers2) {
        this.numbers2 = numbers2;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
