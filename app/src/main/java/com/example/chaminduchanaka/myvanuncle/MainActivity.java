package com.example.chaminduchanaka.myvanuncle;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.myvanuncle.fragment.ChildrenListFragment;
import com.example.chaminduchanaka.myvanuncle.fragment.DriverProfileFragment;
import com.example.chaminduchanaka.myvanuncle.fragment.DropOffFragment;
import com.example.chaminduchanaka.myvanuncle.fragment.PickUpFragment;
import com.example.chaminduchanaka.myvanuncle.fragment.SchoolListFragment;
import com.example.chaminduchanaka.myvanuncle.helper.LocaleHelper;
import com.example.chaminduchanaka.myvanuncle.service.SaveData;
import com.example.chaminduchanaka.myvanuncle.service.UsefullData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.CREATE_LOG;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DRIVER_ID;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.DRIVER_NAME;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.MOBILE;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.PASSWORD;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.TODAY_CHILDREN_LIST;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private TextView toolbar_title;
    private Button btn_pick;
    private Button btn_pick_evening;
    private LinearLayout ll_buttons;

    private SaveData saveData;
    private UsefullData usefullData;
    RequestQueue requestQueue;

    LocationManager locationManager;

    boolean gps_enabled = false;
    boolean network_enabled = false;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        Bugsnag.init(this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText("My Van Uncle");

        saveData = new SaveData(this);
        usefullData = new UsefullData(this);
        requestQueue = Volley.newRequestQueue(this);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
        toggle.setHomeAsUpIndicator(R.drawable.menu);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //change the overflow menu icon
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.logo_icon);
        toolbar.setOverflowIcon(drawable);

        btn_pick = findViewById(R.id.btn_pick);
        btn_pick_evening = findViewById(R.id.btn_pick_evening);
        ll_buttons = findViewById(R.id.ll_buttons);

        Context context = LocaleHelper.onAttach(this);
        final Resources resources = context.getResources();

        btn_pick.setText(resources.getString(R.string.morning));
        btn_pick_evening.setText(resources.getString(R.string.evening));

        Menu menu = navigationView.getMenu();
        MenuItem nav_dashboard = menu.findItem(R.id.dashboard);
        MenuItem nav_children = menu.findItem(R.id.add_child);
        MenuItem nav_school = menu.findItem(R.id.add_school);
        MenuItem nav_driver_profile = menu.findItem(R.id.driver_profile);
        MenuItem nav_settings = menu.findItem(R.id.settings);
        MenuItem nav_logout = menu.findItem(R.id.logout);

        nav_dashboard.setTitle(resources.getString(R.string.dashboard));
        nav_children.setTitle(resources.getString(R.string.children));
        nav_school.setTitle(resources.getString(R.string.school));
        nav_driver_profile.setTitle(resources.getString(R.string.driver_profile));
        nav_settings.setTitle(resources.getString(R.string.settings));
        nav_logout.setTitle(resources.getString(R.string.logout));

        btn_pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //add fragment
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.frame, new PickUpFragment(), "PICK_UP");
                transaction.commit();

                //hide the button
                ll_buttons.setVisibility(View.GONE);
            }
        });

        btn_pick_evening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //add fragment
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.frame, new DropOffFragment(), "DROP_OFF");
                transaction.commit();

                //hide the button
                ll_buttons.setVisibility(View.GONE);

                //crate log
                createLog(saveData.retrieveString(DRIVER_NAME) + " started evening drive");
            }
        });

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //check gps is enabled
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        Log.e("GPS", "" + gps_enabled);
        Log.e("Network", "" + network_enabled);

        if (!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("GPS Not Enabled");
            dialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if (id == R.id.add_child) {

            ll_buttons.setVisibility(View.GONE);

            transaction.replace(R.id.frame, new ChildrenListFragment(), "CHILDREN_LIST");
            transaction.commit();

        } else if (id == R.id.add_school) {

            ll_buttons.setVisibility(View.GONE);

            transaction.replace(R.id.frame, new SchoolListFragment(), "SCHOOL_LIST");
            transaction.commit();

        } else if (id == R.id.dashboard) {

            if (manager.findFragmentByTag("CHILDREN_LIST") != null
                    || manager.findFragmentByTag("SCHOOL_LIST") != null
                    || manager.findFragmentByTag("DRIVER_PROFILE") != null) {
                transaction.remove(getSupportFragmentManager().findFragmentById(R.id.frame)).commit();
                ll_buttons.setVisibility(View.VISIBLE);
            }

        } else if (id==R.id.driver_profile) {

            ll_buttons.setVisibility(View.GONE);

            transaction.replace(R.id.frame, new DriverProfileFragment(), "DRIVER_PROFILE");
            transaction.commit();

        } else if (id == R.id.logout) {
            Intent i=new Intent(this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
            startActivity(i);
            finish();

            saveData.saveString(DRIVER_ID, "");
            saveData.saveString(DRIVER_NAME, "");
            saveData.saveString(MOBILE, "");
            saveData.saveString(PASSWORD, "");
            saveData.saveString(TODAY_CHILDREN_LIST, "");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void createLog(final String msg) {
        String url = BASE_URL + CREATE_LOG;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")) {
//                                usefullData.displayMsg(message);
                            } else {
//                                usefullData.displayMsg(message);
                            }
                        } catch (JSONException e) {
                            Log.e("registerUser()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("driver_id", saveData.retrieveString(DRIVER_ID));
                params.put("activity", msg);

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

}
