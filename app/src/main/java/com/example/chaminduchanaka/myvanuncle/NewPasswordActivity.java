package com.example.chaminduchanaka.myvanuncle;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.chaminduchanaka.myvanuncle.helper.LocaleHelper;
import com.example.chaminduchanaka.myvanuncle.service.SaveData;
import com.example.chaminduchanaka.myvanuncle.service.UsefullData;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.chaminduchanaka.myvanuncle.service.Constance.BASE_URL;
import static com.example.chaminduchanaka.myvanuncle.service.Constance.RESET_PASSWORD;

public class NewPasswordActivity extends AppCompatActivity {

    private TextView tv_login;
    private TextView tv_remember;

    private EditText et_new_password;
    private EditText et_re_enter_new_password;

    private Button btn_reset;

    private RotateLoading rotateloading;

    private UsefullData usefullData;
    private RequestQueue requestQueue;
    private SaveData saveData;

    private String driver_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);

        driver_id = getIntent().getStringExtra("driver_id");

        usefullData = new UsefullData(this);
        requestQueue = Volley.newRequestQueue(this);
        saveData = new SaveData(this);

        tv_login = findViewById(R.id.tv_login);
        tv_remember = findViewById(R.id.tv_remember);

        et_new_password = findViewById(R.id.et_new_password);
        et_re_enter_new_password = findViewById(R.id.et_re_enter_new_password);

        btn_reset = findViewById(R.id.btn_reset);

        rotateloading = findViewById(R.id.rotateloading);

        tv_login.setPaintFlags(tv_login.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        Context context = LocaleHelper.onAttach(this);
        final Resources resources = context.getResources();

        et_new_password.setHint(resources.getString(R.string.new_password));
        et_re_enter_new_password.setHint(resources.getString(R.string.re_enternew_password));
        btn_reset.setText(resources.getString(R.string.reset_password));
        tv_remember.setText(resources.getString(R.string.remember));
        tv_login.setText(resources.getString(R.string.login));

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_new_password.getText().toString().trim().equals("")) {
                    usefullData.displayMsg(resources.getString(R.string.new_password_required));
                } else {
                    if (et_new_password.getText().toString().trim().equals(et_re_enter_new_password.getText().toString().trim())) {
                        rotateloading.start();
                        resetPassword();
                    } else {
                        usefullData.showErrorDialog(resources.getString(R.string.passwords_didnt_match));
                    }
                }

            }
        });

    }

    private void resetPassword() {
        String url = BASE_URL + RESET_PASSWORD;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            String message = jsonObject.getString("message");
                            if (success.equals("1")) {
                                rotateloading.stop();
                                usefullData.displayMsg(message);
                                finish();
                            } else {
                                rotateloading.stop();
                                usefullData.showErrorDialog(message);
                            }
                        } catch (JSONException e) {
                            Log.e("registerUser()", "Json Error");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        rotateloading.stop();
                        if (error instanceof NetworkError) {
                            usefullData.showBottomSheetDialog("Network error occured..!");
                        } else if (error instanceof ServerError) {
                            usefullData.showBottomSheetDialog("Server error occured..!");
                        } else if (error instanceof AuthFailureError) {
                            usefullData.showBottomSheetDialog("Authentication failure..!");
                        } else if (error instanceof NoConnectionError) {
                            usefullData.showBottomSheetDialog("No internet connection..!");
                        } else {
                            usefullData.showBottomSheetDialog("Unknown error occured..!");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("driver_id", driver_id);
                params.put("driver_passowrd", et_new_password.getText().toString().trim());

                Log.e("Params", "" + params);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
